import React, { Component } from 'react';
import { createAppContainer, createStackNavigator, createSwitchNavigator, createBottomTabNavigator } from 'react-navigation';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { fromRight, fromTop } from 'react-navigation-transitions'
import MapboxGL from '@react-native-mapbox-gl/maps';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import FAIcon from 'react-native-vector-icons/FontAwesome';
import AntIcon from 'react-native-vector-icons/AntDesign';

// Redux
import configureStore from './app/redux/store';

const { store, persistor } = configureStore();

// Set access token Mapbox
import { MAPBOX_API_KEY } from './app/services/MapboxClient';

MapboxGL.setAccessToken(MAPBOX_API_KEY);

// Bottomtab
import BottomNavigator from './app/components/BottomNavigator';

import History from './app/screens/bottomtab/History';
import Home from './app/screens/bottomtab/Home';
import Profile from './app/screens/bottomtab/Profile';

// Menu
import PickupTrash from './app/screens/menu/PickupTrash';
import QRCode from './app/screens/menu/QRCode';
import Withdraw from './app/screens/menu/Withdraw';

// Map
import Navigation from './app/screens/map/Navigation';
import PickupLocation from './app/screens/map/PickupLocation';
import SearchDropPoint from './app/screens/map/SearchDropPoint';

// Auth
import Login from './app/screens/auth/Login';
import Register from './app/screens/auth/Register';

// Welcome
import SplashScreen from './app/screens/boarding/SplashScreen';
import WelcomePage from './app/screens/boarding/WelcomePage';

// Process
import ProcessWithdraw from './app/screens/process/ProcessWithdraw';
import PickupScheduled from './app/screens/process/PickupScheduled';
import DeliveryTrash from './app/screens/process/DeliveryTrash';

const AppHomeBottomTabNavigator = createBottomTabNavigator({
  Riwayat: {
    screen: History,
    navigationOptions: {
      tabBarIcon: ({ tintColor }) => (
        <MaterialIcon name='history' size={24} color={tintColor} />
      )
    }
  },
  Beranda: {
    screen: Home,
    navigationOptions: {
      tabBarIcon: ({ tintColor }) => (
        <AntIcon name='home' size={24} color={tintColor} />
      )
    }
  },
  Profil: {
    screen: Profile,
    navigationOptions: {
      tabBarIcon: ({ tintColor }) => (
        <FAIcon name='user-o' size={24} color={tintColor} />
      )
    }
  }
}, {
  initialRouteName: 'Beranda',
  tabBarOptions: {
    activeTintColor: 'white',
    inactiveTintColor: 'grey',
  },
  tabBarComponent: BottomNavigator
});

const AppHomeStackNavigator = createStackNavigator({
  AppHomeBottomTabNavigator: AppHomeBottomTabNavigator,
  PickupTrash: PickupTrash,
  Navigation: Navigation,
  PickupLocation: PickupLocation,
  SearchDropPoint: SearchDropPoint,
  QRCode: QRCode,
  Withdraw: Withdraw,
  ProcessWithdraw: ProcessWithdraw,
  PickupScheduled: PickupScheduled,
  DeliveryTrash: DeliveryTrash
}, {
  initialRouteName: 'AppHomeBottomTabNavigator',
  headerMode: 'none',
  transitionConfig: () => fromRight(500)
});

const AppAuthStackNavigator = createStackNavigator({
  Login: Login,
  Register: Register
}, {
  initialRouteName: 'Login',
  headerMode: 'none',
  transitionConfig: () => fromTop(500)
});

const AppHomeSwitchNavigator = createSwitchNavigator({
  SplashScreen: SplashScreen,
  WelcomePage: WelcomePage,
  AppAuthStackNavigator: AppAuthStackNavigator,
  AppHomeStackNavigator: AppHomeStackNavigator,
}, {
  initialRouteName: 'SplashScreen'
});

const AppContainer = createAppContainer(AppHomeSwitchNavigator);

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <PersistGate location={null} persistor={persistor}>
          <AppContainer />
        </PersistGate>
      </Provider>
    );
  }
}