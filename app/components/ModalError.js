import React, { Component } from 'react';
import { View, Text, Image, Dimensions } from 'react-native';
import Modal from 'react-native-modal';
import IonIcon from 'react-native-vector-icons/Ionicons';

const { width, height } = Dimensions.get('window');

class ModalError extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    onToggleModal = () => {
        const { callback } = this.props;

        if (callback) {
            callback(true);
        } else {
            callback(false);
        }
    }

    render() {
        const { isVisible } = this.props;
        return (
            <Modal
                isVisible={isVisible}
                swipeDirection={['down']}
                onSwipeComplete={() => this.onToggleModal()}
                onBackButtonPress={() => this.onToggleModal()}
                style={{ justifyContent: 'flex-end', margin: 0 }}
            >
                <View style={{ padding: 15, backgroundColor: 'white' }}>
                    <View style={{ alignItems: 'baseline' }}>
                        <IonIcon name='md-close' size={24} color='#333' onPress={() => this.onToggleModal()} />
                    </View>
                    <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                        <Image source={require('../images/undraw/cancel.png')} style={{ width: (width) / 1.5, height: (width) / 2.2, marginBottom: 15 }} />
                        <Text style={{ fontSize: 18, fontFamily: 'Montserrat-Bold', color: '#333', textAlign: 'center' }}>Sepertinya kami memiliki masalah</Text>
                    </View>
                </View>
            </Modal>
        );
    }
}

export default ModalError;
