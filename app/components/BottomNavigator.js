import React, { Component } from 'react';
import { View, Text, Dimensions, Animated } from 'react-native';
import { TouchableRipple } from 'react-native-paper';
import * as Animatable from 'react-native-animatable';

const { width, height } = Dimensions.get('window');

class BottomNavigator extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: new Animated.Value(0)
        };
    }
    
    onIndexChange = (route) => {
        const { onTabPress } = this.props;

        this.aniRef.bounceOut(1000);
        Animated.timing(
            this.state.value, {
                toValue: 1,
                duration: 200
            }
        ).start(() => {
            onTabPress({ route });
            this.aniRef.bounceIn(1000);
            Animated.timing(
                this.state.value, {
                    toValue: 0,
                    duration: 200
                }
            ).start();
        });
    }

    render() {
        const { value } = this.state;
        const { renderIcon, getLabelText, activeTintColor, inactiveTintColor, navigation } = this.props;
        const { routes, index: activeRouteIndex } = navigation.state;
        
        const backgroundColor = value.interpolate({
            inputRange: [0, 1],
            outputRange: ['#2ECC71', 'white']
        });

        return (
            <View style={{ width, height: 60, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-around', backgroundColor: 'white', padding: 15, elevation: 20 }}>
                {
                    routes.map((route, routeIndex) => {
                        const isRouteActive = routeIndex === activeRouteIndex;
                        const tintColor = isRouteActive ? activeTintColor : inactiveTintColor;

                        return (
                            <View key={routeIndex}>
                                {
                                    isRouteActive
                                        ?   <Animatable.View ref={ref => this.aniRef = ref}>
                                                <Animated.View style={{ width: width / 3.5, height: 40, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-around', backgroundColor, borderRadius: 20, padding: 5 }}>
                                                    {renderIcon({ route, focused: isRouteActive, tintColor })}
                                                    <Text style={{ fontSize: 15, fontFamily: 'Montserrat-Bold', color: 'white' }}>{getLabelText({ route })}</Text>
                                                </Animated.View>
                                            </Animatable.View>
                                        :   <View style={{ width: 40, height: 40, borderRadius: 60 }}>
                                                <TouchableRipple
                                                    onPress={() => this.onIndexChange(route)}
                                                    onLongPress={() => this.onIndexChange(route)}
                                                    rippleColor='#333'
                                                    borderless={true}
                                                    style={{ width: '100%', height: '100%', borderRadius: 30, alignItems: 'center', justifyContent: 'center' }}
                                                >
                                                    {renderIcon({ route, focused: isRouteActive, tintColor })}
                                                </TouchableRipple>
                                            </View>
                                }
                            </View>
                        )
                    })
                }
            </View>
        );
    }
}

export default BottomNavigator;