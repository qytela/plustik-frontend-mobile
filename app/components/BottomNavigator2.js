import React, { PureComponent } from 'react';
import { View, Text, Dimensions, Animated, StyleSheet, TouchableOpacity } from 'react-native';
import * as shape from 'd3-shape';
import { Svg, Path } from 'react-native-svg';

import StaticTabbar from './StaticTabbar';

const AnimatedSvg = Animated.createAnimatedComponent(Svg);

const { width } = Dimensions.get('window');
const tabWidth = width / 5;
const height = 64;

const tabs = [
    {
        name: "grid",
    },
    {
        name: "map",
    },
    {
        name: "repeat",
    },
    {
        name: "user",
    },
    {
        name: "repeat",
    }
];

const getPath = () => {
    const left = shape.line().x(d => d.x).y(d => d.y)([
        { x: 0, y: 0 },
        { x: width, y: 0 },
    ]);
    const tab = shape.line().x(d => d.x).y(d => d.y).curve(shape.curveBasis)([
        { x: width, y: 0 },
        { x: width + 5, y: 0 },
        { x: width + 10, y: 10 },
        { x: width + 15, y: 40 },
        { x: width + tabWidth - 15, y: 40 },
        { x: width + tabWidth - 10, y: 10 },
        { x: width + tabWidth - 5, y: 0 },
        { x: width + tabWidth, y: 0 },
    ]);
    const right = shape.line().x(d => d.x).y(d => d.y)([
        { x: width + tabWidth, y: 0 },
        { x: width * 2, y: 0 },
        { x: width * 2, y: height },
        { x: 0, y: height },
        { x: 0, y: 0 },
    ]);
    return `${left} ${tab} ${right}`;
};
const d = getPath();
const backgroundColor = '#2ECC71';

class BottomNavigator extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            value: new Animated.Value(0)
        };
    }

    render() {
        const { value } = this.state;
        const translateX = value.interpolate({
            inputRange: [0, width],
            outputRange: [-width, 0]
        });
        return (
            <>
                <View {...{ height, width }} style={{ position: 'absolute', bottom: 0 }}>
                    <AnimatedSvg width={width * 2} {...{ height }} style={{ transform: [{ translateX }] }}>
                        <Path fill={backgroundColor} {...{ d }} />
                    </AnimatedSvg>
                    <View style={StyleSheet.absoluteFill}>
                        <StaticTabbar {...{ tabs, value }} />
                    </View>
                </View>
            </>
        );
    }
}

export default BottomNavigator;
