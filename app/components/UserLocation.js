import React, { Component } from 'react';
import { View, ToastAndroid } from 'react-native';
import { connect } from 'react-redux';
import MapboxGL from '@react-native-mapbox-gl/maps';

import { ReqPermissionLocation } from '../services/Permissions';

class UserLocation extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            authorizedLocation: false
        }
    }

    componentDidMount() {
        setTimeout(() => {
            this.onUserLocationOpen();
        }, 1000);
    }

    onUserLocationOpen = async () => {
        try {
            const { callback, showToast } = this.props;

            const reqPermission = await ReqPermissionLocation();
            if (reqPermission.latitude && reqPermission.longitude) {
                this.props.dispatch({ type: 'UPDATE_LOCATION', longitude: reqPermission.longitude, latitude: reqPermission.latitude });
                this.setState({ authorizedLocation: true });
            } else if (reqPermission === false) {
                if (callback) {
                    callback(false);
                }

                this.handleToast({ message: 'Izinkan akses lokasi', showToast });
            } else {
                if (callback) {
                    callback(false);
                }

                this.handleToast({ message: reqPermission.message, showToast });
            }
        } catch (error) {
            alert(JSON.stringify(error));
        }
    }

    handleToast = ({ message, showToast = true }) => {
        if (!showToast) return;

        ToastAndroid.show(
            message,
            ToastAndroid.BOTTOM,
            ToastAndroid.LONG
        );
    }

    handleUserLocation = () => {
        return this.state.authorizedLocation
            ?   this.renderUserLocation()
            :   <View />
    }

    renderUserLocation = () => {
        const { callback, ...props } = this.props;

        if (callback) {
            callback(true);
        }
        
        return (
            <MapboxGL.UserLocation
                {...props}
            />
        );
    }

    render() {
        return (
            this.handleUserLocation()
        );
    }
}

export default connect()(UserLocation);