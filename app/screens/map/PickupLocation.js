import React, { Component } from 'react';
import { View, Text, Dimensions, StatusBar, SafeAreaView, TouchableOpacity, TouchableWithoutFeedback, TextInput, Modal, BackHandler, ToastAndroid } from 'react-native';
import { Container, Card, CardItem, Body } from 'native-base';
import { connect } from 'react-redux';
import MapboxGL from '@react-native-mapbox-gl/maps';
import IonIcon from 'react-native-vector-icons/Ionicons';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import NetInfo from '@react-native-community/netinfo';

import UserLocation from '../../components/UserLocation';
import ModalInternet from '../../components/ModalInternet';
import ModalError from '../../components/ModalError';
import { ForwardGeocoding } from '../../services/MapboxClient';

const { width, height } = Dimensions.get('window');

class PickupLocation extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            modalVisibleInternet: false,
            modalVisibleError: false,
            search_text: '',
            data: [],
            setData: {}
        };
    }

    componentDidMount() {
        typeof this.props.data.coordinates !== 'undefined' ? this.setState({ setData: this.props.data }) : this.setState({ setData: {} });

        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackHandlerPress();
            return true;
        });
    }
    
    componentWillUnmount() {
        this.backHandler.remove();
    }
    
    onBackHandlerPress = () => {
        this.mapViewRef.takeSnap(false)
        .then(uri => {
            let data = {};

            data['coordinates'] = this.state.setData.coordinates;
            data['place_name'] = this.state.setData.place_name;
            data['uri'] = uri;

            this.props.dispatch({ type: 'UPDATE_DATA', data });
            this.props.navigation.goBack();
        });
    }
    
    onLocationPress = ({ coordinates, place_name }) => {
        let { setData } = this.state;

        setData['coordinates'] = coordinates;
        setData['place_name'] = place_name;

        this.setState({
            modalVisible: false,
            setData
        });
    }

    onRegionDidChange = async ({ current_location = false, region = {} }) => {
        try {
            if (this.props.location.myLocation.length === 0) {
                return ToastAndroid.show(
                    'Izinkan akses lokasi',
                    ToastAndroid.BOTTOM,
                    ToastAndroid.LONG
                );
            }

            const resJson = await ForwardGeocoding(current_location ? this.props.location.myLocation : region.geometry.coordinates);
            if (resJson.features.length > 0) {
                this.setState({ data: resJson.features });
                this.onLocationPress({ coordinates: resJson.features[0].center, place_name: resJson.features[0].place_name });
            }
        } catch (error) {
            const isConnected = await NetInfo.isConnected.fetch();
            if (!isConnected) {
                return this.setState({ modalVisibleInternet: true });
            }

            this.setState({ modalVisibleError: true });
        }
    }

    onToggleModalInternet = (e) => {
        if (e === true) {
            this.setState({ modalVisibleInternet: false });
        }
    }

    onToggleModalError = (e) => {
        if (e === true) {
            this.setState({ modalVisibleError: false });
        }
    }

    handleSearchText = async () => {
        try {
            const { search_text } = this.state;

            if (search_text === '') return;

            const resJson = await ForwardGeocoding(this.state.search_text);
            this.setState({ data: resJson.features });
        } catch (error) {
            return;
        }
    }

    renderModal = () => {
        this.handleSearchText();

        return (
            <View style={{ padding: 15 }}>
                <View style={{  flexDirection: 'row', justifyContent: 'space-between' }}>
                    <Text style={{ fontSize: 18, fontFamily: 'Montserrat-Bold', color: '#333' }}>Cari lokasimu kamu</Text>
                    <IonIcon name='md-close' size={24} color='#333' onPress={() => this.setState({ modalVisible: false })} />
                </View>
                <TextInput
                    placeholder='Cari lokasi'
                    onChangeText={(search_text) => this.setState({ search_text })}
                    style={{ width: '100%', height: (width) / 8, backgroundColor: 'white', elevation: 2, borderRadius: 10, padding: 10, marginTop: 10, fontSize: 18, fontFamily: 'Montserrat-Bold', color: '#333' }}
                />
                <TouchableOpacity activeOpacity={0.8} onPress={() => this.onRegionDidChange({ current_location: true })}>
                    <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 20 }}>
                        <MaterialIcon name='my-location' size={30} color='blue' />
                        <Text style={{ fontSize: 18, fontFamily: 'Montserrat-Bold', color: '#333', marginLeft: 10 }}>Gunakan lokasi saat ini</Text>
                    </View>
                </TouchableOpacity>
                {
                    this.state.data.map((item, index) => (
                        <TouchableOpacity key={index} activeOpacity={0.8} onPress={() => this.onLocationPress({ coordinates: item.center, place_name: item.place_name })}>
                            <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 20, marginRight: 20 }}>
                                <MaterialIcon name='location-on' size={30} color='#EA4141' />
                                <Text style={{ fontSize: 18, fontFamily: 'Montserrat-Bold', color: '#333', marginLeft: 10 }}>{item.place_name}</Text>
                            </View>
                        </TouchableOpacity>
                    ))
                }
            </View>
        );
    }

    renderPointAnnotation = () => (
        Object.values(this.state.setData).length > 0
            ?   <MapboxGL.PointAnnotation
                    key={0}
                    id={this.state.setData.place_name}
                    coordinate={this.state.setData.coordinates}
                >
                    <MapboxGL.Callout
                        title={this.state.setData.place_name}
                        textStyle={{ fontFamily: 'Montserrat-Bold', color: '#333' }}
                    />
                </MapboxGL.PointAnnotation>
            :   <View />
    )

    render() {
        return (
            <Container>
                <StatusBar
                    backgroundColor='#2ECC71'
                    barStyle='light-content'
                />
                <ModalInternet
                    isVisible={this.state.modalVisibleInternet}
                    callback={this.onToggleModalInternet}
                />
                <ModalError
                    isVisible={this.state.modalVisibleError}
                    callback={this.onToggleModalError}
                />
                <Modal
                    animationType='slide'
                    visible={this.state.modalVisible}
                    onRequestClose={() => this.setState({ modalVisible: false })}
                >
                    {this.renderModal()}
                </Modal>
                <SafeAreaView style={{ flex: 1, marginTop: StatusBar.currentHeight }}>
                    <View style={{ width: '100%', height: (width) / 8, backgroundColor: '#2ECC71' }}>
                        <View style={{ alignItems: 'center', justifyContent: 'space-between', flexDirection: 'row', padding: 15 }}>
                            <IonIcon name='ios-arrow-back' size={24} color='white' onPress={() => this.onBackHandlerPress()} />
                            <Text style={{ fontSize: 18, fontFamily: 'Montserrat-Bold', color: 'white' }}>Lokasi Penjemputan</Text>
                            <Text style={{ fontSize: 18, fontFamily: 'Montserrat-Bold', color: 'white' }} />
                        </View>
                    </View>
                    <View style={{ position: 'absolute', zIndex: 1, left: 0, right: 0, top: 60 }}>
                        <Card style={{ borderRadius: 20, marginLeft: 15, marginRight: 15 }}>
                            <TouchableWithoutFeedback onPress={() => this.setState({ modalVisible: true })}>
                                <CardItem style={{ borderRadius: 20, justifyContent: 'space-between' }}>
                                    <Text style={{ fontSize: 15, fontFamily: 'Montserrat-Bold', color: '#333' }}>Cari lokasimu saat ini...</Text>
                                    <IonIcon name='ios-search' size={24} color='#333' />
                                </CardItem>
                            </TouchableWithoutFeedback>
                        </Card>
                    </View>
                    <MapboxGL.MapView
                        ref={(ref) => this.mapViewRef = ref}
                        showUserLocation={true}
                        compassEnabled={false}
                        logoEnabled={false}
                        attributionEnabled={false}
                        userTrackingMode={1}
                        // onRegionDidChange={(region) => this.onRegionDidChange({ region })}
                        style={{ flex: 1 }}
                    >
                        <MapboxGL.Camera
                            zoomLevel={20}
                            animationMode='moveTo'
                            animationDuration={6000}
                            centerCoordinate={Object.values(this.state.setData).length > 0 ? this.state.setData.coordinates : this.props.location.myLocation.length > 0 ? this.props.location.myLocation : null}
                        />
                        <UserLocation
                            animated={true}
                        />
                        {this.renderPointAnnotation()}
                    </MapboxGL.MapView>
                    {
                        Object.values(this.state.setData).length > 0
                            ?   <View style={{ position: 'absolute', left: 0, right: 0, bottom: 30 }}>
                                    <Card style={{ borderRadius: 20, marginLeft: 15, marginRight: 15 }}>
                                        <CardItem style={{ borderTopLeftRadius: 20, borderTopRightRadius: 20 }}>
                                            <Body style={{ alignItems: 'center', justifyContent: 'center' }}>
                                                <Text style={{ fontSize: 15, fontFamily: 'Montserrat-Bold', color: '#2ECC71', textAlign: 'center' }}>Alamat Penjemputan</Text>
                                            </Body>
                                        </CardItem>
                                        <CardItem style={{ borderBottomLeftRadius: 20, borderBottomRightRadius: 20 }}>
                                            <Body style={{ alignItems: 'center', justifyContent: 'center' }}>
                                                <Text style={{ fontSize: 15, fontFamily: 'Montserrat-Bold', color: '#333', textAlign: 'center' }}>{this.state.setData.place_name}</Text>
                                            </Body>
                                        </CardItem>
                                    </Card>
                                </View>
                            :   <View />
                    }
                </SafeAreaView>
            </Container>
        );
    }
}

const mapStateToProps = state => {
    return {
        data: state.data,
        location: state.location
    };
}

export default connect(mapStateToProps)(PickupLocation);

