import React, { Component } from 'react';
import { View, Text, Dimensions, StatusBar, SafeAreaView, BackHandler } from 'react-native';
import { Container, Card, CardItem, Left, Body } from 'native-base';
import { lineString } from '@turf/helpers';
import { connect } from 'react-redux';
import * as Animatable from 'react-native-animatable';
import MapboxGL from '@react-native-mapbox-gl/maps';
import Moment from 'moment';
import AntIcon from 'react-native-vector-icons/AntDesign';
import IonIcon from 'react-native-vector-icons/Ionicons';

import UserLocation from '../../components/UserLocation';
import ModalLocation from '../../components/ModalLocation';
import { RetrieveDirection } from '../../services/MapboxClient';
import { Distance } from '../../services/Helpers';

const { width, height } = Dimensions.get('window');

/**
 * State:
 * coordinate: [lng, lat]
 */

class Navigation extends Component {
    constructor(props) {
        super(props);
        this.state = {
            onOffDistanceCard: false,
            showView: false,
            modalVisible: false,
            route: null,
            destinationData: {
                id: 0,
                dropPointName: this.props.navigation.getParam('name'),
                dropPointAddress: this.props.navigation.getParam('address'),
                coordinate: this.props.navigation.getParam('coordinate'),
                distance: 0
            }
        };
    }
    
    componentDidMount() {
        const { destinationData } = this.state;
        const { myLocation } = this.props.location;

        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.props.navigation.navigate('AppHomeBottomTabNavigator');
            return true;
        });

        this.handleDistance({ lat1: destinationData.coordinate[1], lng1: destinationData.coordinate[0], lat2: myLocation[1], lng2: myLocation[0] });
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    onToggleModal = (e) => {
        if (e === true) {
            this.setState({ modalVisible: false });
            this.props.navigation.goBack();
        }
    }

    onOpenDistanceCard = () => {
        this.openDistanceRef.fadeOutDownBig()
        .then(() => {
            this.setState({ onOffDistanceCard: false });
            this.cardNavigationRef.fadeInUpBig();
            this.closeDistanceRef.fadeInUpBig();
        });
    }

    onCloseDistanceCard = () => {
        this.closeDistanceRef.fadeOutDownBig()
        .then(() => {
            this.setState({ onOffDistanceCard: true });
            this.cardNavigationRef.fadeOutDownBig();
            this.openDistanceRef.fadeInUpBig();
        });
    }

    handleDistance = ({ lat1, lng1, lat2, lng2 }) => {
        const { destinationData } = this.state;

        const distance = Distance({ lat1, lng1, lat2, lng2 });
        destinationData.distance = distance;

        this.setState({ destinationData });
    }

    handleLineString = async (e) => {
        try {
            if (e === true) {
                const postUrl = await RetrieveDirection('driving', this.props.location.myLocation, this.state.destinationData.coordinate, 'geojson');
                if (postUrl.routes.length > 0) {
                    setTimeout(() => {
                        this.setState({
                            showView: true,
                            route: lineString(postUrl.routes[0].geometry.coordinates)
                        });
                    }, 500);
                }
            } else if (e === false) {
                this.setState({ modalVisible: true });
            }
        } catch (error) {
            alert(JSON.stringify(error));
        }
    }

    renderRouteNavigation = () => (
        this.state.route !== null
            ?   <MapboxGL.ShapeSource id='routeSource' shape={this.state.route}>
                    <MapboxGL.LineLayer id='routeFill' style={{ lineColor: '#EA4141', lineWidth: 5 }} />
                </MapboxGL.ShapeSource>
            :   <View />
    )

    renderCardDistance = () => (
        this.state.onOffDistanceCard
            ?   <Animatable.View ref={(ref) => this.openDistanceRef = ref}>
                    <View style={{ width: (width) / 6, height: (width) / 6, alignItems: 'center', justifyContent: 'center', marginLeft: 15, marginTop: 15, borderRadius: 60, backgroundColor: 'white' }}>
                        <AntIcon name='exclamationcircleo' size={30} color='#333' onPress={() => this.onOpenDistanceCard()} />
                    </View>
                </Animatable.View>
            :   <Animatable.View ref={(ref) => this.closeDistanceRef = ref}>
                    <Card style={{ borderRadius: 20, marginLeft: 15, marginRight: 15, marginTop: 15 }}>
                        <CardItem style={{ borderRadius: 20 }}>
                            <Left>
                                <AntIcon name='close' size={30} color='#333' onPress={() => this.onCloseDistanceCard()} />
                                <Body style={{ alignItems: 'center', justifyContent: 'center' }}>
                                    <Text style={{ fontSize: 18, fontFamily: 'Montserrat-Bold', color: '#333' }}>Berangkat</Text>
                                    <Text style={{ fontSize: 15, fontFamily: 'Montserrat-SemiBold', color: '#333' }}>{this.state.destinationData.distance} - {Moment().format('HH:mm:ss a').toUpperCase()}</Text>
                                </Body>
                            </Left>
                        </CardItem>
                    </Card>
                </Animatable.View>
    )

    renderCardNavigation = () => (
        this.state.showView
            ?   <View style={{ position: 'absolute', left: 0, right: 0, bottom: 30 }}>
                    <Animatable.View ref={(ref) => this.cardNavigationRef = ref}>
                        <Card style={{ borderRadius: 20, marginLeft: 15, marginRight: 15 }}>
                            <CardItem style={{ borderRadius: 20 }}>
                                <Left>
                                    <Animatable.Text animation='bounceInDown'>
                                        <AntIcon name='arrowup' size={30} color='#333' />
                                    </Animatable.Text>
                                    <Body style={{ alignItems: 'center', justifyContent: 'center' }}>
                                        <Animatable.Text animation='bounceInUp' duration={2000}>
                                            <Text style={{ fontSize: 18, fontFamily: 'Montserrat-Bold', color: '#333' }}>Menuju</Text>
                                        </Animatable.Text>
                                        <Animatable.Text animation='bounceInUp' duration={2000}>
                                            <Text style={{ fontSize: 15, fontFamily: 'Montserrat-SemiBold', color: '#333', textAlign: 'center' }}>{this.state.destinationData.dropPointAddress}</Text>
                                        </Animatable.Text>
                                    </Body>
                                </Left>
                            </CardItem>
                        </Card>
                    </Animatable.View>
                    {this.renderCardDistance()}
                </View>
            :   <View />
    )

    render() {
        return (
            <Container>
                <StatusBar
                    backgroundColor='#2ECC71'
                    barStyle='light-content'
                />
                <ModalLocation
                    isVisible={this.state.modalVisible}
                    callback={this.onToggleModal}
                />
                <SafeAreaView style={{ flex: 1, marginTop: StatusBar.currentHeight }}>
                    <View style={{ width: '100%', height: (width) / 8, backgroundColor: '#2ECC71' }}>
                        <View style={{ alignItems: 'center', justifyContent: 'space-between', flexDirection: 'row', padding: 15 }}>
                            <Text style={{ fontSize: 18, fontFamily: 'Montserrat-Bold', color: 'white' }} />
                            <Text style={{ fontSize: 18, fontFamily: 'Montserrat-Bold', color: 'white' }}>Navigasi</Text>
                            <IonIcon name='md-close' size={24} color='white' onPress={() => this.props.navigation.navigate('AppHomeBottomTabNavigator')} />
                        </View>
                    </View>
                    <MapboxGL.MapView
                        ref={(ref) => this.mapViewRef = ref}
                        showUserLocation={true}
                        compassEnabled={false}
                        logoEnabled={false}
                        attributionEnabled={false}
                        userTrackingMode={1}
                        onUserLocationUpdate={(region) => alert(JSON.stringify(region))}
                        style={{ flex: 1 }}
                    >
                        <MapboxGL.Camera
                            zoomLevel={20}
                            animationMode='flyTo'
                            animationDuration={6000}
                            centerCoordinate={this.props.location.myLocation.length > 0 ? this.props.location.myLocation : null}
                        />
                        <UserLocation
                            animated={true}
                            showToast={false}
                            callback={this.handleLineString}
                        />
                        <MapboxGL.PointAnnotation
                            key={0}
                            id={this.state.destinationData.dropPointName}
                            coordinate={this.state.destinationData.coordinate}
                        >
                            <MapboxGL.Callout
                                title={this.state.destinationData.dropPointName}
                                textStyle={{ fontFamily: 'Montserrat-Bold', color: '#333' }}
                            />
                        </MapboxGL.PointAnnotation>
                        {this.renderRouteNavigation()}
                    </MapboxGL.MapView>
                    {this.renderCardNavigation()}
                </SafeAreaView>
            </Container>
        );
    }
}

const mapStateToProps = state => {
    return {
        location: state.location
    };
}

export default connect(mapStateToProps)(Navigation);
