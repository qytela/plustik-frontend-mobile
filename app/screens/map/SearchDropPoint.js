import React, { Component } from 'react';
import { View, Text, Dimensions, StatusBar, SafeAreaView, BackHandler } from 'react-native';
import { Container, Card, CardItem, Body } from 'native-base';
import { TouchableRipple } from 'react-native-paper';
import { connect } from 'react-redux';
import * as Animatable from 'react-native-animatable';
import MapboxGL from '@react-native-mapbox-gl/maps';
import ShimmerPlaceHolder from 'react-native-shimmer-placeholder';
import Carousel from 'react-native-snap-carousel';
import IonIcon from 'react-native-vector-icons/Ionicons';
import AsyncStorage from '@react-native-community/async-storage';
import NetInfo from '@react-native-community/netinfo';

import UserLocation from '../../components/UserLocation';
import ModalLocation from '../../components/ModalLocation';
import ModalError from '../../components/ModalError';
import ModalInternet from '../../components/ModalInternet';
import { GraphQL } from '../../services/API';

const { width, height } = Dimensions.get('window');

/**
 * State:
 * centerCoordinate: [lng, lat]
 * coordinate: [lng, lat]
 */

class SearchDropPoint extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            modalVisibleLocation: false,
            modalVisibleInternet: false,
            modalVisibleError: false,
            centerCoordinate: [],
            dummyData: [
                {
                    id: 0,
                    dropPointName: 'Indomaret A',
                    dropPointAddress: 'Jln.Bahagia Nomor 30, RT 003/009 Grogol, Jakarta Pusat, 17887',
                    coordinate: [
                        106.7877062,
                        -6.2084905
                    ]
                }
            ],
            entries: []
        };
    }

    componentDidMount() {
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            setTimeout(() => {
                this.props.navigation.goBack();
            }, 500);
            return true;
        });
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    onSearchDropPointOpen = async () => {
        try {
            const query = `
                query {
                    drop_points {
                        id
                        name
                        address
                        latitude
                        longitude
                        createdAt
                        updatedAt
                    }
                }
            `;
            const resJson = await GraphQL({ token: await AsyncStorage.getItem('TOKEN'), query });
            
            this.setState({
                isLoading: false,
                centerCoordinate: this.state.dummyData[0].coordinate,
                entries: resJson.data.drop_points
            });
        } catch (error) {
            const isConnected = await NetInfo.isConnected.fetch();
            if (!isConnected) {
                return this.setState({
                    isLoading: false,
                    modalVisibleInternet: true
                });
            }

            this.setState({
                isLoading: false,
                modalVisibleError: true
            });
        }
    }

    onToggleModalLocation = (e) => {
        if (e === true) {
            this.setState({ modalVisibleLocation: false });
            setTimeout(() => {
                this.props.navigation.goBack();
            }, 500);
        }
    }

    onToggleModalInternet = (e) => {
        if (e === true) {
            this.setState({ modalVisibleInternet: false });
            setTimeout(() => {
                this.props.navigation.goBack();
            }, 500);
        }
    }

    onToggleModalError = (e) => {
        if (e === true) {
            this.setState({ modalVisibleError: false });
            setTimeout(() => {
                this.props.navigation.goBack();
            }, 500);
        }
    }

    onDeliveryTrashPress = async ({ id, name, latitude, longitude, address }) => {
        try {
            const query = `
                mutation {
                    send_trash(id: ${id}) {
                        status
                    }
                }
            `;
            const resJson = await GraphQL({ token: await AsyncStorage.getItem('TOKEN'), query });
            if (resJson.data.send_trash.status === true) {
                this.props.navigation.navigate({
                    routeName: 'DeliveryTrash',
                    key: 'DeliveryTrash',
                    params: {
                        name,
                        longitude,
                        latitude,
                        address,
                        createdAt: new Date(),
                        with_menu: true,
                    }
                });
            }
        } catch (error) {
            const isConnected = await NetInfo.isConnected.fetch();
            if (!isConnected) {
                return this.setState({ modalVisibleInternet: true });
            }

            this.setState({ modalVisibleError: true });
        }
    }

    handleCardRender = (e) => {
        if (e === true) {
            this.onSearchDropPointOpen();
        } else if (e === false) {
            this.setState({ modalVisibleLocation: true });
        }
    }

    renderPointAnnotation = () => (
        this.state.entries.map((item, index) => (
            <MapboxGL.PointAnnotation
                key={index}
                id={item.name}
                coordinate={[item.longitude, item.latitude]}
            >
                <MapboxGL.Callout
                    title={item.name}
                    textStyle={{ fontFamily: 'Montserrat-Bold', color: '#333' }}
                />
            </MapboxGL.PointAnnotation>
        ))
    )

    renderShimmerCarousel = () => (
        <Card style={{ borderRadius: 20, marginLeft: 15, marginRight: 15 }}>
            <CardItem style={{ borderTopLeftRadius: 20, borderTopRightRadius: 20 }}>
                <Body style={{ alignItems: 'center', justifyContent: 'center' }}>
                    <ShimmerPlaceHolder
                        autoRun={true}
                    
                        style={{ width: '50%', height: 15, borderRadius: 10 }}
                    />
                </Body>
            </CardItem>
            <CardItem>
                <Body style={{ alignItems: 'center', justifyContent: 'center' }}>
                    <ShimmerPlaceHolder
                        autoRun={true}
                        style={{ width: '80%', height: 15, borderRadius: 10 }}
                    />
                    <ShimmerPlaceHolder
                        autoRun={true}
                        style={{ width: '60%', height: 15, marginTop: 5, borderRadius: 10 }}
                    />
                    <ShimmerPlaceHolder
                        autoRun={true}
                        style={{ width: '40%', height: 15, marginTop: 5, borderRadius: 10 }}
                    />
                </Body>
            </CardItem>
            <CardItem style={{ borderBottomLeftRadius: 20, borderBottomRightRadius: 20 }}>
                <Body style={{ alignItems: 'center', justifyContent: 'center' }}>
                    <ShimmerPlaceHolder
                        autoRun={true}
                        style={{ width: '50%', height: 40, marginTop: 5, borderRadius: 10 }}
                    />
                </Body>
            </CardItem>
        </Card>
    )

    renderCardCarousel = ({ item, index }) => (
        <Animatable.View animation='flipInX' duration={1000}>
            <Card style={{ borderRadius: 20, marginLeft: 15, marginRight: 15 }}>
                <CardItem style={{ borderTopLeftRadius: 20, borderTopRightRadius: 20 }}>
                    <Body style={{ alignItems: 'center', justifyContent: 'center' }}>
                        <Text style={{ fontSize: 15, fontFamily: 'Montserrat-Bold', color: '#333', textAlign: 'center' }}>{item.name}</Text>
                    </Body>
                </CardItem>
                <CardItem>
                    <Body style={{ alignItems: 'center', justifyContent: 'center' }}>
                        <Text style={{ fontSize: 15, fontFamily: 'Montserrat-Bold', color: '#333', textAlign: 'center' }}>{item.address}</Text>
                    </Body>
                </CardItem>
                <CardItem style={{ borderBottomLeftRadius: 20, borderBottomRightRadius: 20 }}>
                    <Body style={{ width, height: (width) / 8, alignItems: 'center', justifyContent: 'center' }}>
                        <TouchableRipple
                            onPress={() => this.onDeliveryTrashPress({ id: item.id, name: item.name, latitude: item.latitude, longitude: item.longitude, address: item.address })}
                            rippleColor='#333'
                            borderless={true}
                            style={{ width: '70%', height: '100%', borderRadius: 20, alignItems: 'center', justifyContent: 'center', backgroundColor: '#2ECC71' }}
                        >
                            <Text style={{ fontSize: 15, fontFamily: 'Montserrat-Bold', color: 'white', textAlign: 'center' }}>Antar Sampah</Text>
                        </TouchableRipple>
                    </Body>
                </CardItem>
            </Card>
        </Animatable.View>
    )

    render() {
        return (
            <Container>
                <StatusBar
                    backgroundColor='#2ECC71'
                    barStyle='light-content'
                />
                <ModalLocation
                    isVisible={this.state.modalVisibleLocation}
                    callback={this.onToggleModalLocation}
                />
                <ModalInternet
                    isVisible={this.state.modalVisibleInternet}
                    callback={this.onToggleModalInternet}
                />
                <ModalError
                    isVisible={this.state.modalVisibleError}
                    callback={this.onToggleModalError}
                />
                <SafeAreaView style={{ flex: 1, marginTop: StatusBar.currentHeight }}>
                    <View style={{ width: '100%', height: (width) / 8, backgroundColor: '#2ECC71' }}>
                        <View style={{ alignItems: 'center', justifyContent: 'space-between', flexDirection: 'row', padding: 15 }}>
                            <IonIcon name='ios-arrow-back' size={24} color='white' onPress={() => this.props.navigation.goBack()} />
                            <Text style={{ fontSize: 18, fontFamily: 'Montserrat-Bold', color: 'white' }}>Cari Drop Point Terdekat</Text>
                            <Text style={{ fontSize: 18, fontFamily: 'Montserrat-Bold', color: 'white' }} />
                        </View>
                    </View>
                    <MapboxGL.MapView
                        ref={(ref) => this.mapViewRef = ref}
                        showUserLocation={true}
                        compassEnabled={false}
                        logoEnabled={false}
                        attributionEnabled={false}
                        userTrackingMode={1}
                        style={{ flex: 1 }}
                    >
                        <MapboxGL.Camera
                            zoomLevel={20}
                            animationMode='flyTo'
                            animationDuration={6000}
                            centerCoordinate={this.state.centerCoordinate.length > 0 ? this.state.centerCoordinate : null}
                        />
                        <UserLocation
                            animated={true}
                            showToast={false}
                            callback={this.handleCardRender}
                        />
                        {this.renderPointAnnotation()}
                    </MapboxGL.MapView>
                    <View style={{ position: 'absolute', left: 0, right: 0, bottom: 0 }}>
                        {
                            this.state.isLoading
                                ?   this.renderShimmerCarousel()
                                :   <Carousel
                                        data={this.state.entries}
                                        renderItem={this.renderCardCarousel}
                                        sliderWidth={(width)}
                                        itemWidth={(width)}
                                        onSnapToItem={(index) => this.setState({ centerCoordinate: [this.state.entries[index].longitude, this.state.entries[index].latitude] })}
                                    />
                        }
                    </View>
                </SafeAreaView>
            </Container>
        );
    }
}

const mapStateToProps = state => {
    return {
        location: state.location
    };
}

export default connect(mapStateToProps)(SearchDropPoint);
