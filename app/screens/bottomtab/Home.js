import React, { Component } from 'react';
import { View, Text, Dimensions, ScrollView, Image, StatusBar, TouchableOpacity, YellowBox } from 'react-native';
import { Container } from 'native-base';
import { connect } from 'react-redux';
import SocketIO from 'socket.io-client';
import AsyncStorage from '@react-native-community/async-storage';
import NetInfo from '@react-native-community/netinfo';

import ModalInternet from '../../components/ModalInternet';
import ModalError from '../../components/ModalError';
import { BASE_URL, GraphQL } from '../../services/API';

YellowBox.ignoreWarnings([
    'WebSocket'
]);

const { width, height } = Dimensions.get('window');

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            modalVisibleInternet: false,
            modalVisibleError: false,
        };

        this.socket = SocketIO.connect(BASE_URL);
    }

    async componentDidMount() {
        this.navigationDidFocusListener = this.props.navigation.addListener('didFocus', () => {
            StatusBar.setBackgroundColor('transparent');
            StatusBar.setBarStyle('dark-content');
            StatusBar.setTranslucent(true);

            this.onHomeOpen();
        });
        
        this.socket.emit('onUserConnect', { token: await AsyncStorage.getItem('TOKEN') });
    }

    componentWillUnmount() {
        this.navigationDidFocusListener.remove();
    }

    onHomeOpen = async () => {
        try {
            const { fullname, address } = this.props.user;

            if (typeof fullname !== 'undefined' && typeof address !== 'undefined') {
                return this.setState({ isLoading: false });
            }

            const query = `
                query {
                    user {
                        profile_user {
                            fullname
                            address
                        }
                    }
                }
            `;
            const resJson = await GraphQL({ token: await AsyncStorage.getItem('TOKEN'), query });

            let data = {};

            data['fullname'] = resJson.data.user.profile_user.fullname;
            data['address'] = resJson.data.user.profile_user.address;

            this.props.dispatch({ type: 'UPDATE_USER', data });
            this.setState({ isLoading: false });
        } catch (error) {
            const isConnected = await NetInfo.isConnected.fetch();
            if (!isConnected) {
                return this.setState({
                    isLoading: false,
                    modalVisibleInternet: true
                });
            }

            this.setState({
                isLoading: false,
                modalVisibleError: true
            });
        }
    }

    onToggleModalInternet = (e) => {
        if (e === true) {
            this.setState({ modalVisibleInternet: false });
        }
    }

    onToggleModalError = (e) => {
        if (e === true) {
            this.setState({ modalVisibleError: false });
        }
    }

    render() {
        return (
            <Container>
                <ModalInternet
                    isVisible={this.state.modalVisibleInternet}
                    callback={this.onToggleModalInternet}
                />
                <ModalError
                    isVisible={this.state.modalVisibleError}
                    callback={this.onToggleModalError}
                />
                <ScrollView
                    showsVerticalScrollIndicator={false}
                >
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <Image source={require('../../images/home/person-welcome.png')} style={{ width: (width) / 2, height: (width) / 2, left: (width) / 2 }} />
                        <View style={{ position: 'absolute', padding: 15, marginTop: 40, flexDirection: 'column' }}>
                            <Text style={{ fontSize: 18, fontFamily: 'Montserrat-Bold', color: '#333' }}>Hai, {this.state.isLoading ? '...' : `${this.props.user.fullname}`}</Text>
                            <Text style={{ fontSize: 15, fontFamily: 'Montserrat-SemiBold', color: '#333' }}>Mulai hari ini kamu dengan.</Text>
                        </View>
                    </View>
                    <View style={{ marginTop: 20 }}>
                        <TouchableOpacity activeOpacity={1} onPress={() => this.props.navigation.navigate('PickupTrash')}>
                            <View style={{ width: '90%', height: (width) / 3 }}>
                                <Image source={require('../../images/home/recyle1.png')} style={{ width: '100%', height: '100%', borderTopRightRadius: 30, borderBottomRightRadius: 30 }} />
                                <View style={{ position: 'absolute', padding: 15, }}>
                                    <Text style={{ fontSize: 18, fontFamily: 'Montserrat-Bold', color: 'white' }}>Jemput Sampah</Text>
                                    <Text style={{ fontSize: 15, fontFamily: 'Montserrat-SemiBold', color: 'white', textAlign: 'left' }}>Petugas kami akan menjemput sampah kamu</Text>
                                </View>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={{ marginTop: 20, marginHorizontal: -5 }}>
                        <TouchableOpacity activeOpacity={1} onPress={() => this.props.navigation.navigate('SearchDropPoint')}>
                            <View style={{ width: '89%', height: (width) / 3 }}>
                                <Image source={require('../../images/home/recyle2.png')} style={{ width: '100%', height: '100%', borderTopRightRadius: 30, borderBottomRightRadius: 30 }} />
                                <View style={{ position: 'absolute', padding: 15, marginLeft: 5 }}>
                                    <Text style={{ fontSize: 18, fontFamily: 'Montserrat-Bold', color: 'white' }}>Setor Sampah</Text>
                                    <Text style={{ fontSize: 15, fontFamily: 'Montserrat-SemiBold', color: 'white', textAlign: 'left' }}>Datangi drop point terdekat kami</Text>
                                </View>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={{ marginTop: 20, marginBottom: 25 }}>
                        <View style={{ width: '90%', height: (width) / 3 }}>
                            <Image source={require('../../images/home/recyle1.png')} style={{ width: '100%', height: '100%', borderTopRightRadius: 30, borderBottomRightRadius: 30 }} />
                            <View style={{ position: 'absolute', padding: 15, }}>
                                <Text style={{ fontSize: 18, fontFamily: 'Montserrat-Bold', color: 'white' }}>Kenali Sampahmu</Text>
                                <Text style={{ fontSize: 15, fontFamily: 'Montserrat-SemiBold', color: 'white', textAlign: 'left' }}>Belajar mengenali sampah kamu sendiri</Text>
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </Container>
        );
    }
}

const mapStateToProps = state => {
    return {
        user: state.user
    };
}

export default connect(mapStateToProps)(Home);
