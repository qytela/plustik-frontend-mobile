import React, { Component } from 'react';
import { View, Text, Dimensions, ScrollView, RefreshControl, StatusBar, Image, SafeAreaView } from 'react-native';
import { Container } from 'native-base';
import { connect } from 'react-redux';
import { TouchableRipple } from 'react-native-paper';
import { PieChart } from 'react-native-chart-kit';
import IonIcon from 'react-native-vector-icons/Ionicons';
import AsyncStorage from '@react-native-community/async-storage';
import NetInfo from '@react-native-community/netinfo';

import ModalInternet from '../../components/ModalInternet';
import ModalError from '../../components/ModalError';
import { GraphQL } from '../../services/API';

const { width, height } = Dimensions.get('window');

class Profile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            modalVisibleInternet: false,
            modalVisibleError: false,
            refreshing: false,
            profileUser: {}
        };
    }

    componentDidMount() {
        this.navigationDidFocusListener = this.props.navigation.addListener('didFocus', () => {
            StatusBar.setBackgroundColor('#2ECC71');
            StatusBar.setBarStyle('light-content');

            this.onProfileOpen();
        });
    }

    componentWillUnmount() {
        this.navigationDidFocusListener.remove();
    }

    onProfileOpen = async () => {
        try {
            const query = `
                query {
                    user {
                        profile_user {
                            saldo
                            points
                            profit
                        }
                    }
                }
            `;
            const resJson = await GraphQL({ token: await AsyncStorage.getItem('TOKEN'), query });
            
            this.setState({
                isLoading: false,
                refreshing: false,
                profileUser: resJson.data.user.profile_user
            });
        } catch (error) {
            const isConnected = await NetInfo.isConnected.fetch();
            if (!isConnected) {
                return this.setState({
                    modalVisibleInternet: true,
                    refreshing: false
                });
            }

            this.setState({
                modalVisibleError: true,
                refreshing: false
            });
        }
    }

    onToggleModalInternet = (e) => {
        if (e === true) {
            this.setState({ modalVisibleInternet: false });
        }
    }
    
    onToggleModalError = (e) => {
        if (e === true) {
            this.setState({ modalVisibleError: false });
        }
    }

    onRefresh = () => {
        this.setState({ refreshing: true });
        this.onProfileOpen();
    }

    onLogoutPress = async () => {
        try {
            await AsyncStorage.removeItem('TOKEN');

            this.props.dispatch({ type: 'CLEAN_DATA' });
            this.props.dispatch({ type: 'CLEAN_USER' });
            this.props.navigation.navigate('AppAuthStackNavigator');
        } catch (error) {
            this.setState({ modalVisibleError: true });
        }
    }

    render() {
        return (
            <Container style={{ backgroundColor: '#F5F6F7' }}>
                <ModalInternet
                    isVisible={this.state.modalVisibleInternet}
                    callback={this.onToggleModalInternet}
                />
                <ModalError
                    isVisible={this.state.modalVisibleError}
                    callback={this.onToggleModalError}
                />
                <SafeAreaView style={{ marginTop: StatusBar.currentHeight }}>
                    <ScrollView
                        showsVerticalScrollIndicator={false}
                        refreshControl={
                            <RefreshControl refreshing={this.state.refreshing} onRefresh={this.onRefresh} />
                        }
                    >
                        <View style={{ width: '100%', height: (width) / 8, backgroundColor: '#2ECC71' }}>
                            <View style={{ alignItems: 'center', justifyContent: 'space-between', flexDirection: 'row', padding: 15 }}>
                                <Text style={{ fontSize: 18, fontFamily: 'Montserrat-Bold', color: 'white' }} />
                                <Text style={{ fontSize: 18, fontFamily: 'Montserrat-Bold', color: 'white' }}>Profil</Text>
                                <IonIcon name='md-exit' size={24} color='white' onPress={() => this.onLogoutPress()} />
                            </View>
                        </View>
                        <View style={{ width: '100%', height: undefined, backgroundColor: 'white' }}>
                            <View style={{ alignItems: 'center', justifyContent: 'center', marginTop: 10, marginBottom: 10 }}>
                                <Image source={{ uri: 'https://thumbor.forbes.com/thumbor/1280x868/https%3A%2F%2Fspecials-images.forbesimg.com%2Fdam%2Fimageserve%2F42977075%2F960x0.jpg%3Ffit%3Dscale' }} style={{ width: (width) / 4, height: (width) / 4, borderRadius: 10 }} />
                                <View style={{ marginTop: 10 }}>
                                    <Text style={{ fontSize: 15, fontFamily: 'Montserrat-Bold', color: '#333', textAlign: 'center' }}>{this.props.user.fullname}</Text>
                                    <Text style={{ fontSize: 15, fontFamily: 'Montserrat-Light', color: 'grey', textAlign: 'center' }}>{this.props.user.address}</Text>
                                </View>
                            </View>
                        </View>
                        <View style={{ width: '100%', height: undefined, marginTop: 10, backgroundColor: 'white' }}>
                            <View style={{ alignItems: 'center', justifyContent: 'center', marginTop: 10, marginBottom: 10 }}>
                                <View style={{ width: (width) / 1.1, height: (width) / 5, justifyContent: 'center', backgroundColor: '#2ECC71', borderRadius: 10, padding: 15 }}>
                                    <View style={{ alignItems: 'center', justifyContent: 'space-between', flexDirection: 'row' }}>
                                        <View style={{ flexDirection: 'column' }}>
                                            <Text style={{ fontSize: 15, fontFamily: 'Montserrat-Bold', color: 'white', textAlign: 'center' }}>Rp {this.state.isLoading ? '0' : this.state.profileUser.saldo}</Text>
                                            <Text style={{ fontSize: 13, fontFamily: 'Montserrat-SemiBold', color: 'white', textAlign: 'center' }}>Saldo</Text>
                                        </View>
                                        <Text style={{ fontSize: 25, fontFamily: 'Montserrat-Light', color: 'white' }}>|</Text>
                                        <View style={{ flexDirection: 'column' }}>
                                            <Text style={{ fontSize: 15, fontFamily: 'Montserrat-Bold', color: 'white', textAlign: 'center' }}>{this.state.isLoading ? '0' : this.state.profileUser.points}</Text>
                                            <Text style={{ fontSize: 13, fontFamily: 'Montserrat-SemiBold', color: 'white', textAlign: 'center' }}>Points</Text>
                                        </View>
                                        <Text style={{ fontSize: 25, fontFamily: 'Montserrat-Light', color: 'white' }}>|</Text>
                                        <View style={{ flexDirection: 'column' }}>
                                            <Text style={{ fontSize: 15, fontFamily: 'Montserrat-Bold', color: 'white', textAlign: 'center' }}>Rp {this.state.isLoading ? '0' : this.state.profileUser.profit}</Text>
                                            <Text style={{ fontSize: 13, fontFamily: 'Montserrat-SemiBold', color: 'white', textAlign: 'center' }}>Keuntungan</Text>
                                        </View>
                                    </View>
                                </View>
                                <View style={{ width: (width) / 1.1, height: (width) / 8, marginTop: 15, backgroundColor: '#353A50', borderRadius: 30 }}>
                                    <TouchableRipple
                                        onPress={() => this.props.navigation.navigate('QRCode', {
                                            fullname: this.props.user.fullname
                                        })}
                                        rippleColor='#333'
                                        borderless={true}
                                        style={{ width: '100%', height: '100%', borderRadius: 30, alignItems: 'center', justifyContent: 'center' }}
                                    >
                                        <Text style={{ fontSize: 15, fontFamily: 'Montserrat-Bold', color: 'white' }}>QR Code Saya</Text>
                                    </TouchableRipple>
                                </View>
                            </View>
                        </View>
                        <View style={{ width: '100%', height: undefined, marginTop: 10, backgroundColor: 'white' }}>
                            <View style={{ alignItems: 'center', justifyContent: 'center', marginTop: 10, marginBottom: 10 }}>
                                <Text style={{ fontSize: 18, fontFamily: 'Montserrat-Bold', color: '#333' }}>7.8kg</Text>
                                <Text style={{ fontSize: 18, fontFamily: 'Montserrat-Light', color: '#333' }}>Total Sampah</Text>
                                <PieChart
                                    data={[
                                        {
                                            name: '',
                                            population: parseFloat(4.08),
                                            color: '#2ECC71',
                                            legendFontColor: '#2ECC71',
                                            legendFontSize: 15
                                        },
                                        {
                                            name: '',
                                            population: parseFloat(1.92),
                                            color: 'cyan',
                                            legendFontColor: 'cyan',
                                            legendFontSize: 15
                                        },
                                        {
                                            name: '',
                                            population: parseFloat(1.8),
                                            color: '#EA4141',
                                            legendFontColor: '#EA4141',
                                            legendFontSize: 15
                                        }
                                    ]}
                                    width={(width)}
                                    height={220}
                                    paddingLeft={15}
                                    accessor='population'
                                    backgroundColor='transparent'
                                    chartConfig={{
                                        color: (opacity = 1) => `rgba(26, 255, 146, ${opacity})`,
                                    }}
                                />
                            </View>
                        </View>
                        <View style={{ width: '100%', height: undefined, marginTop: 10, backgroundColor: 'white' }}>
                            <View style={{ marginTop: 10 }}>
                                <View style={{ flexDirection: 'row', alignItems: 'center', padding: 15 }}>
                                    <View style={{ width: (width) / 20, height: (width) / 20, backgroundColor: '#2ECC71', borderRadius: 60 }} />
                                    <Text style={{ fontSize: 15, fontFamily: 'Montserrat-SemiBold', color: '#333', left: 5 }}>Sampah Botol Plastik</Text>
                                    <View style={{ alignItems: 'flex-end', marginLeft: 'auto' }}>
                                        <Text style={{ fontSize: 15, fontFamily: 'Montserrat-SemiBold', color: '#333', left: 5 }}>4.08kg</Text>
                                    </View>
                                </View>
                            </View>
                            <View style={{ marginTop: 10 }}>
                                <View style={{ flexDirection: 'row', alignItems: 'center', padding: 15 }}>
                                    <View style={{ width: (width) / 20, height: (width) / 20, backgroundColor: 'cyan', borderRadius: 60 }} />
                                    <Text style={{ fontSize: 15, fontFamily: 'Montserrat-SemiBold', color: '#333', left: 5 }}>Gelas Plastik</Text>
                                    <View style={{ alignItems: 'flex-end', marginLeft: 'auto' }}>
                                        <Text style={{ fontSize: 15, fontFamily: 'Montserrat-SemiBold', color: '#333', left: 5 }}>1.92kg</Text>
                                    </View>
                                </View>
                            </View>
                            <View style={{ marginTop: 10 }}>
                                <View style={{ flexDirection: 'row', alignItems: 'center', padding: 15 }}>
                                    <View style={{ width: (width) / 20, height: (width) / 20, backgroundColor: '#EA4141', borderRadius: 60 }} />
                                    <Text style={{ fontSize: 15, fontFamily: 'Montserrat-SemiBold', color: '#333', left: 5 }}>Plastik (Mie, Kopi, dll)</Text>
                                    <View style={{ alignItems: 'flex-end', marginLeft: 'auto' }}>
                                        <Text style={{ fontSize: 15, fontFamily: 'Montserrat-SemiBold', color: '#333', left: 5 }}>1.8kg</Text>
                                    </View>
                                </View>
                            </View>
                        </View>
                        <View style={{ width: '100%', height: undefined, marginTop: 10, marginBottom: 30, backgroundColor: 'white' }}>
                            <View style={{ alignItems: 'center', justifyContent: 'center', marginTop: 10, marginBottom: 10 }}>
                                <View style={{ width: (width) / 1.1, height: (width) / 8, backgroundColor: '#2ECC71', borderRadius: 30 }}>
                                    <TouchableRipple
                                        onPress={() => this.props.navigation.navigate('Withdraw')}
                                        rippleColor='#333'
                                        borderless={true}
                                        style={{ width: '100%', height: '100%', borderRadius: 30, alignItems: 'center', justifyContent: 'center' }}
                                    >
                                        <Text style={{ fontSize: 15, fontFamily: 'Montserrat-Bold', color: 'white' }}>TARIK SALDO</Text>
                                    </TouchableRipple>
                                </View>
                            </View>
                        </View>
                    </ScrollView>
                </SafeAreaView>
            </Container>
        );
    }
}

const mapStateToProps = state => {
    return {
        user: state.user
    };
}

export default connect(mapStateToProps)(Profile);
