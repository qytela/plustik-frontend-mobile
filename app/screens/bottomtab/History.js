import React, { Component } from 'react';
import { View, Text, Dimensions, ScrollView, RefreshControl, StatusBar, Image, TouchableOpacity } from 'react-native';
import { Container, Thumbnail } from 'native-base';
import { SafeAreaView } from 'react-navigation';
import { connect } from 'react-redux';
import ShimmerPlaceHolder from 'react-native-shimmer-placeholder';
import AsyncStorage from '@react-native-community/async-storage';
import NetInfo from '@react-native-community/netinfo';
import Moment from 'moment';

import ModalInternet from '../../components/ModalInternet';
import ModalError from '../../components/ModalError';
import { GraphQL } from '../../services/API';

const { width, height } = Dimensions.get('window');

class History extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            modalVisibleIntenet: false,
            modalVisibleError: false,
            refreshing: false,
            data: []
        };
    }

    componentDidMount() {
        this.navigationDidFocusListener = this.props.navigation.addListener('didFocus', () => {
            StatusBar.setBackgroundColor('#2ECC71');
            StatusBar.setBarStyle('light-content');

            this.setState({ isLoading: true });
            this.onHistoryOpen();
        });
    }

    componentWillUnmount() {
        this.navigationDidFocusListener.remove();
    }

    onHistoryOpen = async () => {
        try {
            const query = `
                query {
                    user {
                        history_user (orderBy: { field: "createdAt", direction: "DESC" }) {
                            id
                            title
                            slug
                            pickup_date
                            pickup_time
                            notes
                            latitude
                            longitude
                            place_name
                            status
                            createdAt
                            updatedAt
                        }
                    }
                }
            `;
            const resJson = await GraphQL({ token: await AsyncStorage.getItem('TOKEN'), query });

            this.setState({
                isLoading: false,
                refreshing: false
            });

            if (resJson.data.user.history_user.length > 0) {
                this.setState({ data: resJson.data.user.history_user });
            }
        } catch (error) {
            const isConnected = await NetInfo.isConnected.fetch();
            if (!isConnected) {
                return this.setState({
                    isLoading: false,
                    modalVisibleIntenet: true,
                    refreshing: false
                });
            }

            this.setState({
                isLoading: false,
                modalVisibleError: true,
                refreshing: false
            });
        }
    }

    onToggleModalInternet = (e) => {
        if (e === true) {
            this.setState({ modalVisibleIntenet: false });
        }
    }

    onToggleModalError = (e) => {
        if (e === true) {
            this.setState({ modalVisibleError: false });
        }
    }

    onRefresh = () => {
        this.setState({ refreshing: true });
        this.onHistoryOpen();
    }

    onHistoryPress = ({ id, status, name, notes, slug, longitude, latitude, address, pickup_date, pickup_time, createdAt, updatedAt }) => {
        if (slug === 'pickup_trash') {
            switch (true) {
                case status === 'wait_pickup':
                    this.props.navigation.navigate({
                        routeName: 'PickupScheduled',
                        key: 'PickupScheduled',
                        params: {
                            id,
                            pickup_date,
                            pickup_time
                        }
                    });
                default:
                    break;
            }
        }

        if (slug === 'send_trash') {
            switch (true) {
                case status === 'wait_send_trash':
                    this.props.navigation.navigate({
                        routeName: 'DeliveryTrash',
                        key: 'DeliveryTrash',
                        params: {
                            id,
                            name,
                            notes,
                            longitude,
                            latitude,
                            address,
                            pickup_date,
                            pickup_time,
                            createdAt,
                            updatedAt
                        }
                    });
                default:
                    break;
            }
        }
    }

    handleParseDateTime = (date) => {
        const returnDate = Moment(parseInt(date)).format('DD MMMM YYYY');
        const returnTime = Moment(parseInt(date)).format('HH:mm:ss a').toUpperCase();

        return `${returnDate} - ${returnTime}`;
    }

    handleStatus = ({ status, slug }) => {
        if (slug === 'pickup_trash') {
            switch (true) {
                case status === 'wait_pickup':
                    return {
                        icon: require('../../images/icons/car-active-icon.png'),
                        backgroundColor: 'orange',
                        text: 'Menunggu Tim Pickup'
                    };
                case status === 'wait_team_pickup':
                    return {
                        icon: require('../../images/icons/car-active-icon.png'),
                        backgroundColor: 'orange',
                        text: 'Telah Diambil Tim Pickup'
                    };
                case status === 'done_pickup':
                    return {
                        icon: require('../../images/icons/car-active-icon.png'),
                        backgroundColor: '#2ECC71',
                        text: 'Selesai Pickup'
                    };
                case status === 'cancel_pickup':
                    return {
                        icon: require('../../images/icons/car-active-icon.png'),
                        backgroundColor: '#EA4141',
                        text: 'Batal Pickup'
                    };
                default:
                    break;
            }
        }

        if (slug === 'send_trash') {
            switch (true) {
                case status === 'wait_send_trash':
                    return {
                        icon: require('../../images/icons/box-active-icon.png'),
                        backgroundColor: 'orange',
                        text: 'Menunggu Mengantar'
                    };
                case status === 'wait_pickup':
                    return {
                        icon: require('../../images/icons/box-active-icon.png'),
                        backgroundColor: 'orange',
                        text: 'Menunggu Tim Pickup'
                    };
                case status === 'wait_team_pickup':
                    return {
                        icon: require('../../images/icons/box-active-icon.png'),
                        backgroundColor: 'orange',
                        text: 'Telah Diambil Tim Pickup'
                    };
                case status === 'cancel_send_trash':
                    return {
                        icon: require('../../images/icons/box-active-icon.png'),
                        backgroundColor: '#EA4141',
                        text: 'Batal Mengantar'
                    };
                case status === 'done_pickup':
                    return {
                        icon: require('../../images/icons/box-active-icon.png'),
                        backgroundColor: '#2ECC71',
                        text: 'Selesai Pickup'
                    };
                default:
                    break;
            }
        }

        if (slug === 'profit') {
            switch (true) {
                case status === 'done_send_profit':
                    return {
                        icon: require('../../images/icons/in-icon.png'),
                        backgroundColor: '#2ECC71',
                        text: 'Saldo Bertambah'
                    };
                default:
                    break;
            }
        }
    }

    renderLoadingHistory = () => (
        <View style={{ padding: 15 }}>
            <View style={{ flexDirection: 'row', marginBottom: 15 }}>
                <View style={{ alignItems: 'center', justifyContent: 'center', marginLeft: 5, marginTop: 15 }}>
                    <ShimmerPlaceHolder
                        autoRun={true}
                        style={{ width: (width) / 6, height: (width) / 6, borderRadius: 60 }}
                    />
                </View>
                <View style={{ flexDirection: 'column', marginLeft: 15 }}>
                    <ShimmerPlaceHolder
                        autoRun={true}
                        style={{ width: '70%', height: 15, borderRadius: 10 }}
                    />
                    <ShimmerPlaceHolder
                        autoRun={true}
                        style={{ width: '85%', height: 15, borderRadius: 10, marginTop: 5 }}
                    />
                    <ShimmerPlaceHolder
                        autoRun={true}
                        style={{ width: (width) / 2, height: (width) / 13, borderRadius: 20, marginTop: 5 }}
                    />
                </View>
            </View>
            <View style={{ flexDirection: 'row', marginBottom: 15 }}>
                <View style={{ alignItems: 'center', justifyContent: 'center', marginLeft: 5, marginTop: 15 }}>
                    <ShimmerPlaceHolder
                        autoRun={true}
                        style={{ width: (width) / 6, height: (width) / 6, borderRadius: 60 }}
                    />
                </View>
                <View style={{ flexDirection: 'column', marginLeft: 15 }}>
                    <ShimmerPlaceHolder
                        autoRun={true}
                        style={{ width: '70%', height: 15, borderRadius: 10 }}
                    />
                    <ShimmerPlaceHolder
                        autoRun={true}
                        style={{ width: '85%', height: 15, borderRadius: 10, marginTop: 5 }}
                    />
                    <ShimmerPlaceHolder
                        autoRun={true}
                        style={{ width: (width) / 2, height: (width) / 13, borderRadius: 20, marginTop: 5 }}
                    />
                </View>
            </View>
            <View style={{ flexDirection: 'row', marginBottom: 15 }}>
                <View style={{ alignItems: 'center', justifyContent: 'center', marginLeft: 5, marginTop: 15 }}>
                    <ShimmerPlaceHolder
                        autoRun={true}
                        style={{ width: (width) / 6, height: (width) / 6, borderRadius: 60 }}
                    />
                </View>
                <View style={{ flexDirection: 'column', marginLeft: 15 }}>
                    <ShimmerPlaceHolder
                        autoRun={true}
                        style={{ width: '70%', height: 15, borderRadius: 10 }}
                    />
                    <ShimmerPlaceHolder
                        autoRun={true}
                        style={{ width: '85%', height: 15, borderRadius: 10, marginTop: 5 }}
                    />
                    <ShimmerPlaceHolder
                        autoRun={true}
                        style={{ width: (width) / 2, height: (width) / 13, borderRadius: 20, marginTop: 5 }}
                    />
                </View>
            </View>
        </View>
    )

    renderHistory = () => (
        <View style={{ height: undefined, padding: 15 }}>
            {
                this.state.data.map((item, index) => (
                    <TouchableOpacity key={index} activeOpacity={1} onPress={() => this.onHistoryPress({
                        id: item.id,
                        status: item.status,
                        name: item.title,
                        slug: item.slug,
                        notes: item.notes,
                        pickup_date: item.pickup_date,
                        pickup_time: item.pickup_time,
                        longitude: item.longitude,
                        latitude: item.latitude,
                        address: item.place_name,
                        createdAt: item.createdAt,
                        updatedAt: item.updatedAt
                    })}>
                        <View style={{ flexDirection: 'row', marginBottom: 15 }}>
                            <Thumbnail large source={this.handleStatus({ status: item.status, slug: item.slug }).icon} />
                            <View style={{ flexDirection: 'column', marginLeft: 5 }}>
                                <Text style={{ fontSize: 15, fontFamily: 'Montserrat-SemiBold', color: '#333' }}>{item.title}</Text>
                                <Text style={{ fontSize: 13, fontFamily: 'Montserrat-SemiBold', color: '#333', marginTop: 5 }}>{this.handleParseDateTime(item.createdAt)}</Text>
                                <View style={{ width: (width) / 2, height: (width) / 13, backgroundColor: this.handleStatus({ status: item.status, slug: item.slug }).backgroundColor, alignItems: 'center', justifyContent: 'center', marginTop: 5, borderRadius: 20 }}>
                                    <Text style={{ fontSize: 13, fontFamily: 'Montserrat-SemiBold', color: 'white', textAlign: 'center' }}>{this.handleStatus({ status: item.status, slug: item.slug }).text}</Text>
                                </View>
                            </View>
                        </View>
                    </TouchableOpacity>
                ))
            }
        </View>
    )

    render() {
        return (
            <Container>
                <ModalInternet
                    isVisible={this.state.modalVisibleIntenet}
                    callback={this.onToggleModalInternet}
                />
                <ModalError
                    isVisible={this.state.modalVisibleError}
                    callback={this.onToggleModalError}
                />
                <SafeAreaView style={{ marginTop: StatusBar.currentHeight }}>
                    <ScrollView
                        showsVerticalScrollIndicator={false}
                        refreshControl={
                            <RefreshControl refreshing={this.state.refreshing} onRefresh={this.onRefresh} />
                        }
                    >
                        <View style={{ width: '100%', height: (width) / 8, backgroundColor: '#2ECC71' }}>
                            <View style={{ alignItems: 'center', justifyContent: 'center', padding: 15 }}>
                                <Text style={{ fontSize: 18, fontFamily: 'Montserrat-Bold', color: 'white' }}>Riwayat</Text>
                            </View>
                        </View>
                        {
                            this.state.isLoading
                                ?   this.renderLoadingHistory()
                                :   this.state.data.length > 0
                                        ?   this.renderHistory()
                                        :   <View style={{ height: (height) - 100, flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                                                <Image source={require('../../images/undraw/no-data.png')} style={{ width: (width) / 1.5, height: (width) / 1.5 }} />
                                                <Text style={{ fontSize: 18, fontFamily: 'Montserrat-Bold', color: '#333' }}>Tidak ada history</Text>
                                            </View>
                        }
                    </ScrollView>
                </SafeAreaView>
            </Container>
        );
    }
}

const mapStateToProps = state => {
    return {
        data: state.data
    };
}

export default connect(mapStateToProps)(History);
