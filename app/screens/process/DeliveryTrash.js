import React, { Component } from 'react';
import { View, Text, Dimensions, StatusBar, Image, ImageBackground, Alert, BackHandler } from 'react-native';
import { Container } from 'native-base';
import { TouchableRipple } from 'react-native-paper';
import AsyncStorage from '@react-native-community/async-storage';
import NetInfo from '@react-native-community/netinfo';
import AntIcon from 'react-native-vector-icons/AntDesign';
import Spinner from 'react-native-loading-spinner-overlay';
import Moment from 'moment';
import SocketIO from 'socket.io-client';

import ModalInternet from '../../components/ModalInternet';
import ModalError from '../../components/ModalError';
import { BASE_URL, GraphQL } from '../../services/API';

const { width, height } = Dimensions.get('window');

class DeliveryTrash extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            modalVisibleIntenet: false,
            modalVisibleError: false
        };

        this.socket = SocketIO.connect(BASE_URL);
    }

    componentDidMount() {
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.props.navigation.navigate('AppHomeBottomTabNavigator');
            return true;
        });
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    onNavigatePress = () => {
        const { name, latitude, longitude, address } = this.props.navigation.state.params;
        
        this.props.navigation.navigate('Navigation', {
            name,
            coordinate: [longitude, latitude],
            address
        });
    }

    onToggleModalInternet = (e) => {
        if (e === true) {
            this.setState({ modalVisibleIntenet: false });
        }
    }

    onToggleModalError = (e) => {
        if (e === true) {
            this.setState({ modalVisibleError: false });
        }
    }

    onDonePress = async () => {
        try {
            const { id, name, notes, latitude, longitude, pickup_date, pickup_time, address, createdAt, updatedAt } = this.props.navigation.state.params;

            this.setState({ isLoading: true });

            const query = `
                mutation {
                    done_send_trash(id: "${id}") {
                        status
                    }
                }
            `;
            const resJson = await GraphQL({ token: await AsyncStorage.getItem('TOKEN'), query });

            this.setState({ isLoading: false });

            if (resJson.data.done_send_trash.status === true) {
                const data = {
                    id,
                    title: name,
                    pickup_date,
                    pickup_time,
                    notes,
                    longitude,
                    latitude,
                    place_name: address,
                    slug: 'send_trash',
                    createdAt,
                    updatedAt
                };
                
                this.socket.emit('onSearchCourier', { token: await AsyncStorage.getItem('TOKEN'), data });
                this.props.navigation.navigate('Riwayat');
            }

            this.props.navigation.navigate('Riwayat');
        } catch (error) {
            const isConnected = await NetInfo.isConnected.fetch();
            if (!isConnected) {
                return this.setState({
                    isLoading: false,
                    modalVisibleIntenet: true
                });
            }

            this.setState({
                isLoading: false,
                modalVisibleError: true
            });
        }
    }

    onCancelPress = async () => {
        try {
            const { id } = this.props.navigation.state.params;

            this.setState({ isLoading: true });

            const query = `
                mutation {
                    cancel_send_trash(id: "${id}") {
                        status
                    }
                }
            `;
            const resJson = await GraphQL({ token: await AsyncStorage.getItem('TOKEN'), query });

            this.setState({ isLoading: false });

            if (resJson.data.cancel_send_trash.status === true) {
                this.props.navigation.navigate('Riwayat');
            }

            this.props.navigation.navigate('Riwayat');
        } catch (error) {
            const isConnected = await NetInfo.isConnected.fetch();
            if (!isConnected) {
                return this.setState({
                    isLoading: false,
                    modalVisibleIntenet: true
                });
            }

            this.setState({
                isLoading: false,
                modalVisibleError: true
            });
        }
    }

    handleParserTime = () => {
        const { with_menu, pickup_time, createdAt } = this.props.navigation.state.params;

        let cvTime = '';
        if (typeof with_menu !== 'undefined') {
            cvTime = Moment(createdAt).add(120, 'm').format('HH:mm:ss');
        } else {
            const getAmPm = parseInt(pickup_time) >= 12 ? 'PM' : 'AM';
            cvTime = `${pickup_time} ${getAmPm}`;
        }
        
        return cvTime;
    }

    render() {
        return (
            <Container>
                <StatusBar
                    backgroundColor='transparent'
                    translucent={true}
                    barStyle='dark-content'
                />
                <ModalInternet
                    isVisible={this.state.modalVisibleIntenet}
                    callback={this.onToggleModalInternet}
                />
                <ModalError
                    isVisible={this.state.modalVisibleError}
                    callback={this.onToggleModalError}
                />
                <Spinner
                    visible={this.state.isLoading}
                    textContent='Loading...'
                    textStyle={{ fontSize: 18, fontFamily: 'Montserrat-Bold', color: 'white' }}
                />
                <ImageBackground source={require('../../images/background1.png')} style={{ width: '100%', height: '100%' }}>
                    <View style={{ alignItems: 'flex-end', justifyContent: 'center', top: 50, right: 10 }}>
                        <AntIcon name='close' size={24} color='#333' onPress={() => this.props.navigation.navigate('AppHomeBottomTabNavigator')} />
                    </View>
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', padding: 15 }}>
                        <Text style={{ fontSize: 20, fontFamily: 'Montserrat-Bold', color: '#333', textAlign: 'center' }}>Antar Sampah</Text>
                        <View style={{ marginTop: 10 }}>
                            <Text style={{ fontSize: 15, fontFamily: 'Montserrat-Light', color: '#333', textAlign: 'center' }}>Antar sampah kamu ke Drop Point yang sudah kami pilih.</Text>
                        </View>
                        <View style={{ marginTop: 20 }}>
                            <Image source={require('../../images/process/recycle-bag.png')} style={{ width: (width) / 2.1, height: (width) / 2.5 }} />
                        </View>
                        <View style={{ marginTop: 15 }}>
                            <Text style={{ fontSize: 15, fontFamily: 'Montserrat-Light', color: '#333', textAlign: 'center' }}>Jam Pengantaran</Text>
                            <Text style={{ fontSize: 15, fontFamily: 'Montserrat-SemiBold', color: '#333', textAlign: 'center' }}>Lakukan Pengantaran Sebelum Pukul</Text>
                            <Text style={{ fontSize: 15, fontFamily: 'Montserrat-SemiBold', color: '#333', textAlign: 'center' }}>{this.handleParserTime()}</Text>
                        </View>
                        <View style={{ marginTop: 5 }}>
                            <Text style={{ fontSize: 15, fontFamily: 'Montserrat-Light', color: '#333', textAlign: 'center' }}>Status</Text>
                            <Text style={{ fontSize: 15, fontFamily: 'Montserrat-SemiBold', color: '#333', textAlign: 'center' }}>Menunggu Pengambilan</Text>
                        </View>
                        <View style={{ marginTop: 30 }}>
                            <View style={{ width: (width) / 1.2, height: (width) / 8, alignItems: 'center', justifyContent: 'center', backgroundColor: '#2ECC71', borderRadius: 30 }}>
                                <TouchableRipple
                                    onPress={() => this.onNavigatePress()}
                                    rippleColor='#333'
                                    borderless={true}
                                    style={{ width: '100%', height: '100%', borderRadius: 30, alignItems: 'center', justifyContent: 'center' }}
                                >
                                    <Text style={{ fontSize: 15, fontFamily: 'Montserrat-Bold', color: 'white', textAlign: 'center' }}>Tunjukan Lokasi</Text>
                                </TouchableRipple>
                            </View>
                        </View>
                        <View style={{ marginTop: 15 }}>
                            <View style={{ width: (width) / 1.2, height: (width) / 8, alignItems: 'center', justifyContent: 'center', backgroundColor: '#2ECC71', borderRadius: 30 }}>
                                <TouchableRipple
                                    onPress={() => this.props.navigation.navigate('Riwayat')}
                                    rippleColor='#333'
                                    borderless={true}
                                    style={{ width: '100%', height: '100%', borderRadius: 30, alignItems: 'center', justifyContent: 'center' }}
                                >
                                    <Text style={{ fontSize: 15, fontFamily: 'Montserrat-Bold', color: 'white', textAlign: 'center' }}>Lihat Riwayat Pesanan Saya</Text>
                                </TouchableRipple>
                            </View>
                        </View>
                        {
                            typeof this.props.navigation.state.params.id !== 'undefined'
                                ?   <View>
                                        <View style={{ marginTop: 30 }}>
                                            <View style={{ width: (width) / 1.2, height: (width) / 8, alignItems: 'center', justifyContent: 'center', backgroundColor: '#2D8BE3', borderRadius: 30 }}>
                                                <TouchableRipple
                                                    onPress={() => Alert.alert(
                                                        'Konfirmasi',
                                                        'Apakah kamu yakin ingin menyelesaikan pengantaran ?',
                                                        [
                                                            { text: 'CANCEL', onPress: () => {} },
                                                            { text: 'OK', onPress: () => this.onDonePress() }
                                                        ]
                                                    )}
                                                    rippleColor='#333'
                                                    borderless={true}
                                                    style={{ width: '100%', height: '100%', borderRadius: 30, alignItems: 'center', justifyContent: 'center' }}
                                                >
                                                    <Text style={{ fontSize: 15, fontFamily: 'Montserrat-Bold', color: 'white', textAlign: 'center' }}>Selesai Mengantar</Text>
                                                </TouchableRipple>
                                            </View>
                                        </View>
                                        <View style={{ marginTop: 15 }}>
                                            <View style={{ width: (width) / 1.2, height: (width) / 8, alignItems: 'center', justifyContent: 'center', backgroundColor: '#EA4141', borderRadius: 30 }}>
                                                <TouchableRipple
                                                    onPress={() => Alert.alert(
                                                        'Konfirmasi',
                                                        'Apakah kamu yakin ingin membatalkan pengantaran ?',
                                                        [
                                                            { text: 'CANCEL', onPress: () => {} },
                                                            { text: 'OK', onPress: () => this.onCancelPress() }                                                        ]
                                                    )}
                                                    rippleColor='#333'
                                                    borderless={true}
                                                    style={{ width: '100%', height: '100%', borderRadius: 30, alignItems: 'center', justifyContent: 'center' }}
                                                >
                                                    <Text style={{ fontSize: 15, fontFamily: 'Montserrat-Bold', color: 'white', textAlign: 'center' }}>Batalkan</Text>
                                                </TouchableRipple>
                                            </View>
                                        </View>
                                    </View>
                                :   <View />
                        }
                    </View>
                </ImageBackground>
            </Container>
        );
    }
}

export default DeliveryTrash;
