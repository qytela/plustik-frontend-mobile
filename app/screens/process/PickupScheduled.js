import React, { Component } from 'react';
import { View, Text, Dimensions, StatusBar, Image, ImageBackground, Alert, BackHandler } from 'react-native';
import { Container } from 'native-base';
import { TouchableRipple } from 'react-native-paper';
import AsyncStorage from '@react-native-community/async-storage';
import NetInfo from '@react-native-community/netinfo';
import Moment from 'moment';
import Spinner from 'react-native-loading-spinner-overlay';
import AntIcon from 'react-native-vector-icons/AntDesign';

import ModalInternet from '../../components/ModalInternet';
import ModalError from '../../components/ModalError';
import { GraphQL } from '../../services/API';

const { width, height } = Dimensions.get('window');

class PickupScheduled extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            modalVisibleIntenet: false,
            modalVisibleError: false
        };
    }

    componentDidMount() {
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.props.navigation.navigate('AppHomeBottomTabNavigator');
            return true;
        });
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    onToggleModalInternet = (e) => {
        if (e === true) {
            this.setState({ modalVisibleIntenet: false });
        }
    }

    onToggleModalError = (e) => {
        if (e === true) {
            this.setState({ modalVisibleError: false });
        }
    }

    onCancelPress = async () => {
        try {
            const { id } = this.props.navigation.state.params;

            this.setState({ isLoading: true });

            const query = `
                mutation {
                    cancel_pickup(id: "${id}") {
                        status
                    }
                }
            `;
            const resJson = await GraphQL({ token: await AsyncStorage.getItem('TOKEN'), query });

            this.setState({ isLoading: false });

            if (resJson.data.cancel_pickup.status === true) {
                this.props.navigation.navigate('Riwayat');
            }

            this.props.navigation.navigate('Riwayat');
        } catch (error) {
            const isConnected = await NetInfo.isConnected.fetch();
            if (!isConnected) {
                return this.setState({
                    isLoading: false,
                    modalVisibleIntenet: true
                });
            }

            this.setState({
                isLoading: false,
                modalVisibleError: true
            });
        }
    }

    handleParserDateTime = () => {
        const { pickup_date, pickup_time } = this.props.navigation.state.params;

        const getAmPm = parseInt(pickup_time) >= 12 ? 'PM' : 'AM';

        const date = Moment(pickup_date).format('DD - MMMM - YYYY');
        const time = `${pickup_time} ${getAmPm}`;

        return {
            date,
            time
        };
    }

    render() {
        return (
            <Container>
                <StatusBar
                    backgroundColor='transparent'
                    translucent={true}
                    barStyle='dark-content'
                />
                <ModalInternet
                    isVisible={this.state.modalVisibleIntenet}
                    callback={this.onToggleModalInternet}
                />
                <ModalError
                    isVisible={this.state.modalVisibleError}
                    callback={this.onToggleModalError}
                />
                <Spinner
                    visible={this.state.isLoading}
                    textContent='Loading...'
                    textStyle={{ fontSize: 18, fontFamily: 'Montserrat-Bold', color: 'white' }}
                />
                <ImageBackground source={require('../../images/background1.png')} style={{ width: '100%', height: '100%' }}>
                    <View style={{ alignItems: 'flex-end', justifyContent: 'center', top: 50, right: 10 }}>
                        <AntIcon name='close' size={24} color='#333' onPress={() => this.props.navigation.navigate('AppHomeBottomTabNavigator')} />
                    </View>
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', padding: 15 }}>
                        <Text style={{ fontSize: 20, fontFamily: 'Montserrat-Bold', color: '#333', textAlign: 'center' }}>Penjemputan Dijadwalkan</Text>
                        <View style={{ marginTop: 10 }}>
                            <Text style={{ fontSize: 15, fontFamily: 'Montserrat-Light', color: '#333', textAlign: 'center' }}>Tunggu Tim kami datang kerumah kamu. Pastikan semua sampah plastik sudah kamu kumpulkan ke dalam recycle bag ya.</Text>
                        </View>
                        <View style={{ marginTop: 20 }}>
                            <Image source={require('../../images/process/hourglass.png')} style={{ width: (width) / 3, height: (width) / 2.5 }} />
                        </View>
                        <View style={{ marginTop: 15 }}>
                            <Text style={{ fontSize: 15, fontFamily: 'Montserrat-Light', color: '#333', textAlign: 'center' }}>Tanggal Penjemputan</Text>
                            <Text style={{ fontSize: 15, fontFamily: 'Montserrat-SemiBold', color: '#333', textAlign: 'center' }}>{this.handleParserDateTime().date}</Text>
                        </View>
                        <View style={{ marginTop: 5 }}>
                            <Text style={{ fontSize: 15, fontFamily: 'Montserrat-Light', color: '#333', textAlign: 'center' }}>Jam Penjemputan</Text>
                            <Text style={{ fontSize: 15, fontFamily: 'Montserrat-SemiBold', color: '#333', textAlign: 'center' }}>{this.handleParserDateTime().time}</Text>
                        </View>
                        <View style={{ marginTop: 5 }}>
                            <Text style={{ fontSize: 15, fontFamily: 'Montserrat-Light', color: '#333', textAlign: 'center' }}>Status</Text>
                            <Text style={{ fontSize: 15, fontFamily: 'Montserrat-SemiBold', color: '#333', textAlign: 'center' }}>Menunggu Pengambilan</Text>
                        </View>
                        <View style={{ marginTop: 30 }}>
                            <View style={{ width: (width) / 1.2, height: (width) / 8, alignItems: 'center', justifyContent: 'center', backgroundColor: '#2ECC71', borderRadius: 30 }}>
                                <TouchableRipple
                                    onPress={() => this.props.navigation.navigate('Riwayat')}
                                    rippleColor='#333'
                                    borderless={true}
                                    style={{ width: '100%', height: '100%', borderRadius: 30, alignItems: 'center', justifyContent: 'center' }}
                                >
                                <Text style={{ fontSize: 15, fontFamily: 'Montserrat-Bold', color: 'white', textAlign: 'center' }}>Lihat Riwayat Pesanan Saya</Text>
                                </TouchableRipple>
                            </View>
                        </View>
                        {
                            typeof this.props.navigation.state.params.id !== 'undefined'
                                ?   <View style={{ marginTop: 30 }}>
                                        <View style={{ width: (width) / 1.2, height: (width) / 8, alignItems: 'center', justifyContent: 'center', backgroundColor: '#EA4141', borderRadius: 30 }}>
                                            <TouchableRipple
                                                onPress={() => Alert.alert(
                                                    'Konfirmasi',
                                                    'Apakah kamu yakin ingin membatalkan pesanan ?',
                                                    [
                                                        { text: 'CANCEL', onPress: () => {} },
                                                        { text: 'OK', onPress: () => this.onCancelPress() }
                                                    ]
                                                )}
                                                rippleColor='#333'
                                                borderless={true}
                                                style={{ width: '100%', height: '100%', borderRadius: 30, alignItems: 'center', justifyContent: 'center' }}
                                            >
                                                <Text style={{ fontSize: 15, fontFamily: 'Montserrat-Bold', color: 'white', textAlign: 'center' }}>Batalkan</Text>
                                            </TouchableRipple>
                                        </View>
                                    </View>
                                :   <View />
                        }
                    </View>
                </ImageBackground>
            </Container>
        );
    }
}

export default PickupScheduled;
