import React, { PureComponent } from 'react';
import { View, Text, Dimensions, StatusBar, Image, ImageBackground, BackHandler } from 'react-native';
import { Container } from 'native-base';
import { TouchableRipple } from 'react-native-paper';
import AntIcon from 'react-native-vector-icons/AntDesign';

const { width, height } = Dimensions.get('window');

class ProcessWithdraw extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    componentDidMount() {
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.props.navigation.navigate('AppHomeBottomTabNavigator');
            return true;
        });
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    render() {
        return (
            <Container>
                <StatusBar
                    backgroundColor='transparent'
                    translucent={true}
                    barStyle='dark-content'
                />
                <ImageBackground source={require('../../images/background1.png')} style={{ width: '100%', height: '100%' }}>
                    <View style={{ alignItems: 'flex-end', justifyContent: 'center', top: 50, right: 10 }}>
                        <AntIcon name='close' size={24} color='#333' onPress={() => this.props.navigation.navigate('AppHomeBottomTabNavigator')} />
                    </View>
                    <View style={{ padding: 15, top: 120 }}>
                        <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                            <Text style={{ fontSize: 20, fontFamily: 'Montserrat-Bold', color: '#333', textAlign: 'center' }}>Penarikan Saldo Berhasil</Text>
                            <View style={{ marginTop: 10 }}>
                                <Text style={{ fontSize: 15, fontFamily: 'Montserrat-Light', color: '#333', textAlign: 'center' }}>Tunggu 1x24 jam untuk pencairan saldo kamu okee.</Text>
                            </View>
                            <View style={{ marginTop: 20 }}>
                                <Image source={require('../../images/process/done.png')} style={{ width: (width) / 3, height: (width) / 3 }} />
                            </View>
                        </View>
                        <View style={{ marginTop: 15, marginLeft: 15, marginRight: 15 }}>
                            <View style={{ alignItems: 'baseline', justifyContent: 'space-between', flexDirection: 'row' }}>
                                <Text style={{ fontSize: 15, fontFamily: 'Montserrat-Bold', color: '#333' }}>Tanggal Penarikan</Text>
                                <Text style={{ fontSize: 15, fontFamily: 'Montserrat-Bold', color: '#333' }}>30 Oktober 2019</Text>
                            </View>
                            <View style={{ alignItems: 'baseline', justifyContent: 'space-between', flexDirection: 'row', marginTop: 5 }}>
                                <Text style={{ fontSize: 15, fontFamily: 'Montserrat-Bold', color: '#333' }}>Jumlah Penarikan</Text>
                                <Text style={{ fontSize: 15, fontFamily: 'Montserrat-Bold', color: '#333' }}>Rp 50.000</Text>
                            </View>
                            <View style={{ alignItems: 'baseline', justifyContent: 'space-between', flexDirection: 'row', marginTop: 5 }}>
                                <Text style={{ fontSize: 15, fontFamily: 'Montserrat-Bold', color: '#333' }}>Status</Text>
                                <Text style={{ fontSize: 15, fontFamily: 'Montserrat-Bold', color: '#333' }}>Menunggu</Text>
                            </View>
                            <View style={{ alignItems: 'baseline', justifyContent: 'space-between', flexDirection: 'row', marginTop: 5 }}>
                                <Text style={{ fontSize: 15, fontFamily: 'Montserrat-Bold', color: '#333' }}>Jenis</Text>
                                <Text style={{ fontSize: 15, fontFamily: 'Montserrat-Bold', color: '#333' }}>Pulsa</Text>
                            </View>
                        </View>
                        <View style={{ marginTop: 30, alignItems: 'center', justifyContent: 'center' }}>
                            <View style={{ width: (width) / 1.2, height: (width) / 8, alignItems: 'center', justifyContent: 'center', backgroundColor: '#2ECC71', borderRadius: 30 }}>
                                <TouchableRipple
                                    onPress={() => alert('Wooo')}
                                    rippleColor='#333'
                                    borderless={true}
                                    style={{ width: '100%', height: '100%', borderRadius: 30, alignItems: 'center', justifyContent: 'center' }}
                                >
                                    <Text style={{ fontSize: 15, fontFamily: 'Montserrat-Bold', color: 'white', textAlign: 'center' }}>Lihat Riwayat Penarikan</Text>
                                </TouchableRipple>
                            </View>
                        </View>
                    </View>
                </ImageBackground>
            </Container>
        );
    }
}

export default ProcessWithdraw;
