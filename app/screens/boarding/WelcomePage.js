import React, { PureComponent } from 'react';
import { View, Text, Image, StatusBar, Dimensions, TouchableOpacity, YellowBox } from 'react-native';
import { Container } from 'native-base';
import { TouchableRipple } from 'react-native-paper';
import Swiper from 'react-native-swiper';
import IonIcon from 'react-native-vector-icons/Ionicons';

YellowBox.ignoreWarnings([
    'ViewPagerAndroid'
]);

const { width, height } = Dimensions.get('window');

class OnBoarding1 extends PureComponent {
    render() {
        return (
            <Container>
                <Image source={require('../../images/onboarding/onboarding-1.png')} style={{ width: '100%', height: '60%' }} />
                <View style={{ position: 'absolute', alignItems: 'center', justifyContent: 'space-around', top: 40, right: 30 }}>
                    <TouchableOpacity onPress={() => this.props.callbackFromChild(+2)}>
                        <Text style={{ fontSize: 15, fontFamily: 'Montserrat-Light', color: '#333' }}>Skip</Text>
                    </TouchableOpacity>
                </View>
                <View style={{ padding: 15 }}>
                    <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                        <Text style={{ fontSize: 20, fontFamily: 'Montserrat-Bold', color: '#333', textAlign: 'center' }}>Kumpulkan Sampah Plastik</Text>
                        <View style={{ marginTop: 20 }}>
                            <Text style={{ fontSize: 15, fontFamily: 'Montserrat-SemiBold', color: '#333', textAlign: 'center' }}>Kumpulkan sampah plastik ke dalam recyle bag yang sudah diberikan oleh Tim Kami.</Text>
                        </View>
                        <View style={{ position: 'absolute', top: 150, bottom: 0 }}>
                            <View style={{ width: (width) / 1.2, height: (width) / 8, alignItems: 'center', justifyContent: 'center', backgroundColor: '#2ECC71', borderRadius: 30 }}>
                                <TouchableRipple
                                    onPress={() => this.props.callbackFromChild(+1)}
                                    rippleColor='#333'
                                    borderless={true}
                                    style={{ width: '100%', height: '100%', borderRadius: 30, alignItems: 'center', justifyContent: 'center' }}
                                >
                                    <Text style={{ fontSize: 20, fontFamily: 'Montserrat-Bold', color: 'white', textAlign: 'center' }}>Selanjutnya</Text>
                                </TouchableRipple>
                            </View>
                            <View style={{ marginTop: 20, alignItems: 'center', justifyContent: 'center' }}>
                                <View style={{ flexDirection: 'row' }}>
                                    <View style={{ width: 10, height: 10, backgroundColor: '#2ECC71', borderRadius: 10 }} />
                                    <View style={{ width: 10, height: 10, backgroundColor: 'grey', borderRadius: 10, marginLeft: 5 }} />
                                    <View style={{ width: 10, height: 10, backgroundColor: 'grey', borderRadius: 10, marginLeft: 5 }} />
                                </View>
                            </View>
                        </View>
                    </View>
                </View>
            </Container>
        );
    }
}

class OnBoarding2 extends PureComponent {
    render() {
        return (
            <Container>
                <Image source={require('../../images/onboarding/onboarding-2.png')} style={{ width: '100%', height: '60%' }} />
                <View style={{ position: 'absolute', alignItems: 'center', justifyContent: 'space-around', top: 40, left: 30 }}>
                    <TouchableOpacity onPress={() => this.props.callbackFromChild(-1)}>
                        <IonIcon name='ios-arrow-back' size={20} color='#333' />
                    </TouchableOpacity>
                </View>
                <View style={{ position: 'absolute', alignItems: 'center', justifyContent: 'space-around', top: 40, right: 30 }}>
                    <TouchableOpacity onPress={() => this.props.callbackFromChild(+1)}>
                        <Text style={{ fontSize: 15, fontFamily: 'Montserrat-Light', color: '#333' }}>Skip</Text>
                    </TouchableOpacity>
                </View>
                <View style={{ padding: 15 }}>
                    <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                        <Text style={{ fontSize: 20, fontFamily: 'Montserrat-Bold', color: '#333', textAlign: 'center' }}>Jadwalkan Pengambilan</Text>
                        <View style={{ marginTop: 20 }}>
                            <Text style={{ fontSize: 15, fontFamily: 'Montserrat-SemiBold', color: '#333', textAlign: 'center' }}>Jika sampah plastik kamu sudah penuh, jadwalkan pengambilan atau pengataran melalui aplikasi.</Text>
                        </View>
                        <View style={{ position: 'absolute', top: 150, bottom: 0 }}>
                            <View style={{ width: (width) / 1.2, height: (width) / 8, alignItems: 'center', justifyContent: 'center', backgroundColor: '#2ECC71', borderRadius: 30 }}>
                                <TouchableRipple
                                    onPress={() => this.props.callbackFromChild(+1)}
                                    rippleColor='#333'
                                    borderless={true}
                                    style={{ width: '100%', height: '100%', borderRadius: 30, alignItems: 'center', justifyContent: 'center' }}
                                >
                                    <Text style={{ fontSize: 20, fontFamily: 'Montserrat-Bold', color: 'white', textAlign: 'center' }}>Selanjutnya</Text>
                                </TouchableRipple>
                            </View>
                            <View style={{ marginTop: 20, alignItems: 'center', justifyContent: 'center' }}>
                                <View style={{ flexDirection: 'row' }}>
                                    <View style={{ width: 10, height: 10, backgroundColor: 'grey', borderRadius: 10 }} />
                                    <View style={{ width: 10, height: 10, backgroundColor: '#2ECC71', borderRadius: 10, marginLeft: 5 }} />
                                    <View style={{ width: 10, height: 10, backgroundColor: 'grey', borderRadius: 10, marginLeft: 5 }} />
                                </View>
                            </View>
                        </View>
                    </View>
                </View>
            </Container>
        );
    }
}

class OnBoarding3 extends PureComponent {
    render() {
        return (
            <Container>
                <Image source={require('../../images/onboarding/onboarding-3.png')} style={{ width: '110%', height: '60%' }} />
                <View style={{ padding: 15 }}>
                    <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                        <Text style={{ fontSize: 20, fontFamily: 'Montserrat-Bold', color: '#333', textAlign: 'center' }}>Tunggu dan Dapatkan Penghasilan</Text>
                        <View style={{ marginTop: 20 }}>
                            <Text style={{ fontSize: 15, fontFamily: 'Montserrat-SemiBold', color: '#333', textAlign: 'center' }}>Tunggu Tim kami datang ke rumah kamu. Dan dapatkan penghasilan tambahan.</Text>
                        </View>
                        <View style={{ position: 'absolute', top: 150, bottom: 0 }}>
                            <View style={{ width: (width) / 1.2, height: (width) / 8, alignItems: 'center', justifyContent: 'center', backgroundColor: '#2ECC71', borderRadius: 30 }}>
                                <TouchableRipple
                                    onPress={() => this.props.callbackFromChild('move')}
                                    rippleColor='#333'
                                    borderless={true}
                                    style={{ width: '100%', height: '100%', borderRadius: 30, alignItems: 'center', justifyContent: 'center' }}
                                >
                                    <Text style={{ fontSize: 20, fontFamily: 'Montserrat-Bold', color: 'white', textAlign: 'center' }}>Mulai Kumpulkan</Text>
                                </TouchableRipple>
                            </View>
                            <View style={{ marginTop: 20, alignItems: 'center', justifyContent: 'center' }}>
                                <View style={{ flexDirection: 'row' }}>
                                    <View style={{ width: 10, height: 10, backgroundColor: 'grey', borderRadius: 10 }} />
                                    <View style={{ width: 10, height: 10, backgroundColor: 'grey', borderRadius: 10, marginLeft: 5 }} />
                                    <View style={{ width: 10, height: 10, backgroundColor: '#2ECC71', borderRadius: 10, marginLeft: 5 }} />
                                </View>
                            </View>
                        </View>
                    </View>
                </View>
            </Container>
        );
    }
}

class WelcomePage extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
        }
    }

    componentDidMount() {
        this.navigationDidFocusListener = this.props.navigation.addListener('didFocus', () => {
            StatusBar.setBackgroundColor('transparent');
            StatusBar.setBarStyle('dark-content');
            StatusBar.setTranslucent(true);
        });
    }

    componentWillUnmount() {
        this.navigationDidFocusListener.remove();
    }

    callbackFromChild = (value) => {
        if (value === 'move') {
            this.props.navigation.navigate('AppAuthStackNavigator');
        } else {
            this.swiperRef.scrollBy(value);
        }
    }

    render() {
        return (
            <Container>
                <Swiper
                    ref={(ref) => this.swiperRef = ref}
                    loop={false}
                    showsPagination={false}
                >
                    <OnBoarding1
                        callbackFromChild={this.callbackFromChild}
                    />
                    <OnBoarding2
                        callbackFromChild={this.callbackFromChild}
                    />
                    <OnBoarding3
                        callbackFromChild={this.callbackFromChild}
                    />
                </Swiper>
            </Container>
        );
    }
}

export default WelcomePage;
