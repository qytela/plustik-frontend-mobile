import React, { PureComponent } from 'react';
import { View } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

class SplashScreen extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
        };
    }
    
    componentWillMount() {
        this.onSplashScreenOpen();
    }

    onSplashScreenOpen = async () => {
        try {
            const TOKEN = await AsyncStorage.getItem('TOKEN');
            if (TOKEN) {
                this.props.navigation.navigate('AppHomeStackNavigator');
            } else {
                this.props.navigation.navigate('AppAuthStackNavigator');
            }
        } catch (error) {
            alert(JSON.stringify(error));
        }
    }

    render() {
        return (
            <View />
        );
    }
}

export default SplashScreen;
