import React, { Component } from 'react';
import { View, Text, Dimensions, StatusBar, SafeAreaView, ScrollView, TouchableOpacity, Image, YellowBox, ToastAndroid, BackHandler } from 'react-native';
import { Container, Textarea } from 'native-base';
import { TouchableRipple } from 'react-native-paper';
import { connect } from 'react-redux';
import MapboxGL from '@react-native-mapbox-gl/maps';
import AsyncStorage from '@react-native-community/async-storage';
import NetInfo from '@react-native-community/netinfo';
import Moment from 'moment';
import DateTimePicker from 'react-native-modal-datetime-picker';
import FontistoIcon from 'react-native-vector-icons/Fontisto';
import SocketIO from 'socket.io-client';

import ModalInternet from '../../components/ModalInternet';
import ModalError from '../../components/ModalError';
import { BASE_URL } from '../../services/API';

YellowBox.ignoreWarnings([
    'WebSocket'
]);

const { width, height } = Dimensions.get('window');

class PickupTrash extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isDatePickerVisible: false,
            isTimePickerVisible: false,
            modalVisibleInternet: false,
            notes: '',
            defaultDate: Moment().format('DD - MMMM - YYYY'),
            defaultDateTwo: Moment().format('YYYY-MM-DD'),
            defaultTime: Moment().format('HH:mm:ss'),
            defaultAmPm: Moment().format('a').toUpperCase()
        };

        this.socket = SocketIO.connect(BASE_URL);
    }

    componentDidMount() {
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onCancelPress();
            return true;
        });
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    onCancelPress = () => {
        this.props.dispatch({ type: 'CLEAN_DATA' });
        this.props.navigation.goBack();
    }

    onToggleModalInternet = (e) => {
        if (e === true) {
            this.setState({ modalVisibleInternet: false });
        }
    }

    handleDatePicker = date => {
        const defaultDate = Moment(date).format('DD - MMMM - YYYY');
        const defaultDateTwo = Moment(date).format('YYYY-MM-DD');
        this.setState({
            defaultDate,
            defaultDateTwo
        });
    }

    handleTimePicker = time => {
        const defaultTime = Moment(time).format('HH:mm:ss');
        const defaultAmPm = Moment(time).format('a').toUpperCase();

        this.setState({
            defaultTime,
            defaultAmPm
        });
    }

    onPickupPress = async () => {
        try {
            const { notes, defaultDate, defaultDateTwo, defaultTime, defaultAmPm } = this.state;
            const { coordinates, place_name } = this.props.data;
    
            if (typeof coordinates === 'undefined' || typeof place_name === 'undefined') {
                return ToastAndroid.show(
                    'Field required',
                    ToastAndroid.BOTTOM,
                    ToastAndroid.LONG
                );
            }

            const isConnected = await NetInfo.isConnected.fetch();
            if (!isConnected) return this.setState({ modalVisibleInternet: true });

            const data = {
                pickup_date: defaultDateTwo,
                pickup_time: defaultTime,
                notes: notes,
                longitude: coordinates[0],
                latitude: coordinates[1],
                place_name: place_name,
                slug: 'pickup_trash',
                createdAt: new Date(),
                updatedAt: new Date()
            };

            this.socket.emit('onSearchCourier', { token: await AsyncStorage.getItem('TOKEN'), data });
            this.props.dispatch({ type: 'CLEAN_DATA' });
            this.props.navigation.navigate({
                routeName: 'PickupScheduled',
                key: 'PickupScheduled',
                params: {
                    pickup_date: defaultDateTwo,
                    pickup_time: defaultTime
                }
            });
        } catch (error) {
            this.setState({ modalVisibleError: true });
        }
    }

    render() {
        return (
            <Container style={{ backgroundColor: '#F5F6F7' }}>
                <StatusBar
                    backgroundColor='transparent'
                    translucent={true}
                    barStyle='dark-content'
                />
                <ModalInternet
                    isVisible={this.state.modalVisibleInternet}
                    callback={this.onToggleModalInternet}
                />
                <ModalError
                    isVisible={this.state.modalVisibleError}
                    callback={this.onToggleModalError}
                />
                <SafeAreaView style={{ marginTop: StatusBar.currentHeight }}>
                    <ScrollView
                        showsVerticalScrollIndicator={false}
                    >
                        <View style={{ flex: 1, alignItems: 'baseline', justifyContent: 'center', marginTop: 20, padding: 15 }}>
                            <Text style={{ fontSize: 15, fontFamily: 'Montserrat-Bold', color: '#333' }}>Pilih Alamat Penjemputan</Text>
                            <View style={{ marginTop: 15 }}>
                                <TouchableOpacity activeOpacity={1} onPress={() => this.props.navigation.navigate('PickupLocation')}>
                                    <View style={{ width: (width) / 1.1, height: (width) / 2, backgroundColor: 'white', elevation: 2, borderRadius: 10 }}>
                                        {
                                            typeof this.props.data.uri !== 'undefined'
                                                ?   <Image source={{ uri: this.props.data.uri }} style={{ width: (width) / 1.1, height: (width) / 2, borderRadius: 10 }} />  
                                                :   <MapboxGL.MapView
                                                        ref={(ref) => this.mapViewRef = ref}
                                                        showUserLocation={true}
                                                        compassEnabled={false}
                                                        logoEnabled={false}
                                                        attributionEnabled={false}
                                                        userTrackingMode={1}
                                                        style={{ width: (width) / 1.1, height: (width) / 2 }}
                                                    />
                                        }
                                    </View>
                                </TouchableOpacity>
                            </View>
                            <View style={{ marginTop: 15 }}>
                                <Text style={{ fontSize: 15, fontFamily: 'Montserrat-Bold', color: '#333' }}>Pilih Tanggal Penjemputan</Text>
                                <View style={{ marginTop: 15 }}>
                                    <TouchableOpacity activeOpacity={1} onPress={() => this.setState({ isDatePickerVisible: !this.state.isDatePickerVisible })}>
                                        <View style={{ width: (width) / 1.6, height: (width) / 6, alignItems: 'center', justifyContent: 'space-between', flexDirection: 'row', backgroundColor: 'white', borderRadius: 10, padding: 15, elevation: 1 }}>
                                            <Text style={{ fontSize: 15, fontFamily: 'Montserrat-SemiBold', color: '#333' }}>{this.state.defaultDate}</Text>
                                            <FontistoIcon name='date' size={24} color='#333' />
                                        </View>
                                    </TouchableOpacity>
                                    <DateTimePicker
                                        mode='date'
                                        isVisible={this.state.isDatePickerVisible}
                                        onConfirm={this.handleDatePicker}
                                        onCancel={() => this.setState({ isDatePickerVisible: !this.state.isDatePickerVisible })}
                                    />
                                </View>
                            </View>
                            <View style={{ marginTop: 15 }}>
                                <Text style={{ fontSize: 15, fontFamily: 'Montserrat-Bold', color: '#333' }}>Pilih Jam Penjemputan</Text>
                                <View style={{ marginTop: 15 }}>
                                    <TouchableOpacity activeOpacity={1} onPress={() => this.setState({ isTimePickerVisible: !this.state.isTimePickerVisible })}>
                                        <View style={{ width: (width) / 1.6, height: (width) / 6, alignItems: 'center', justifyContent: 'space-between', flexDirection: 'row', backgroundColor: 'white', borderRadius: 10, padding: 15, elevation: 1 }}>
                                            <Text style={{ fontSize: 15, fontFamily: 'Montserrat-SemiBold', color: '#333' }}>{this.state.defaultTime} {this.state.defaultAmPm}</Text>
                                            <FontistoIcon name='clock' size={24} color='#333' />
                                        </View>
                                    </TouchableOpacity>
                                    <DateTimePicker
                                        mode='time'
                                        isVisible={this.state.isTimePickerVisible}
                                        onConfirm={this.handleTimePicker}
                                        onCancel={() => this.setState({ isTimePickerVisible: !this.state.isTimePickerVisible })}
                                    />
                                </View>
                            </View>
                            <View style={{ marginTop: 15 }}>
                                <Text style={{ fontSize: 15, fontFamily: 'Montserrat-Bold', color: '#333' }}>Catatan</Text>
                                <View style={{ marginTop: 15 }}>
                                    <Textarea
                                        placeholder='Berikan catatan...'
                                        rowSpan={5}
                                        bordered={true}
                                        onChangeText={(notes) => this.setState({ notes })}
                                        style={{ width: (width) / 1.1, fontSize: 15, fontFamily: 'Montserrat-SemiBold', color: '#333', backgroundColor: 'white', borderRadius: 10 }}
                                    />
                                </View>
                            </View>
                            <View style={{ marginTop: 15, marginBottom: 15 }}>
                                <View style={{ width: (width) / 1.1, height: (width) / 8, marginTop: 15, backgroundColor: '#2ECC71', borderRadius: 30 }}>
                                    <TouchableRipple
                                        onPress={() => this.onPickupPress()}
                                        rippleColor='#333'
                                        borderless={true}
                                        style={{ width: '100%', height: '100%', borderRadius: 30, alignItems: 'center', justifyContent: 'center' }}
                                    >
                                        <Text style={{ fontSize: 15, fontFamily: 'Montserrat-Bold', color: 'white' }}>Jadwalkan Penjemputan</Text>
                                    </TouchableRipple>
                                </View>
                                <View style={{ marginTop: 15 }}>
                                    <View style={{ width: (width) / 1.1, height: (width) / 8, marginTop: 15, backgroundColor: 'transparent', borderRadius: 30, borderColor: '#333', borderWidth: 2 }}>
                                        <TouchableRipple
                                            onPress={() => this.onCancelPress()}
                                            rippleColor='#333'
                                            borderless={true}
                                            style={{ width: '100%', height: '100%', borderRadius: 30, alignItems: 'center', justifyContent: 'center' }}
                                        >
                                            <Text style={{ fontSize: 15, fontFamily: 'Montserrat-Bold', color: '#333' }}>Batalkan</Text>
                                        </TouchableRipple>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </ScrollView>
                </SafeAreaView>
            </Container>
        );
    }
}

const mapStateToProps = state => {
    return {
        data: state.data
    };
}

export default connect(mapStateToProps)(PickupTrash);
