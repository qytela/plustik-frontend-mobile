import React, { PureComponent } from 'react';
import { View, Text, Dimensions, StatusBar, ImageBackground, SafeAreaView } from 'react-native';
import { Container } from 'native-base';
import * as Animatable from 'react-native-animatable';
import QRCode from 'react-native-qrcode-svg';
import IonIcon from 'react-native-vector-icons/Ionicons';

const { width, height } = Dimensions.get('window');

class MyQRCode extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <Container>
                <StatusBar
                    backgroundColor='#2ECC71'
                    barStyle='light-content'
                />
                <SafeAreaView style={{ flex: 1, marginTop: StatusBar.currentHeight }}>
                    <View style={{ width: '100%', height: (width) / 8, backgroundColor: '#2ECC71' }}>
                        <View style={{ alignItems: 'center', justifyContent: 'space-between', flexDirection: 'row', padding: 15 }}>
                            <IonIcon name='ios-arrow-back' size={24} color='white' onPress={() => this.props.navigation.goBack()} />
                            <Text style={{ fontSize: 18, fontFamily: 'Montserrat-Bold', color: 'white' }}>QR Code</Text>
                            <Text style={{ fontSize: 18, fontFamily: 'Montserrat-Bold', color: 'white' }} />
                        </View>
                    </View>
                    <ImageBackground source={require('../../images/background1.png')} style={{ width: '100%', height: '100%' }}>
                        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', padding: 15 }}>
                            <Text style={{ fontSize: 20, fontFamily: 'Montserrat-Bold', color: '#333', textAlign: 'center' }}>QR Code Saya</Text>
                            <View style={{ marginTop: 10 }}>
                                <Text style={{ fontSize: 15, fontFamily: 'Montserrat-Light', color: '#333', textAlign: 'center' }}>Tunjukan QR Code ini ketika ada Tim Plustik datang ke rumah untuk mengambil recycle bag kamu yaa.</Text>
                            </View>
                            <Animatable.View animation='rotate'>
                                <View style={{ marginTop: 20 }}>
                                    <QRCode
                                        value="Just some string value"
                                        size={200}
                                        logo={require('../../images/icons/user-active-icon.png')}
                                        logoSize={50}
                                        logoBackgroundColor='transparent'
                                    />
                                </View>
                            </Animatable.View>
                            <View style={{ marginTop: 20 }}>
                                <Text style={{ fontSize: 20, fontFamily: 'Montserrat-Bold', color: '#333', textAlign: 'center' }}>{this.props.navigation.getParam('fullname')}</Text>
                            </View>
                        </View>
                    </ImageBackground>
                </SafeAreaView>
            </Container>
        );
    }
}

export default MyQRCode;
