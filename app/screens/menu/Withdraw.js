import React, { Component } from 'react';
import { View, Text, Dimensions, ScrollView, StatusBar, TextInput, SafeAreaView } from 'react-native';
import { Container, Item, Picker } from 'native-base';
import { TouchableRipple } from 'react-native-paper';
import IonIcon from 'react-native-vector-icons/Ionicons';

const { width, height } = Dimensions.get('window');

class Withdraw extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectOption: '0'
        };
    }

    render() {
        return (
            <Container style={{ backgroundColor: '#F5F6F7' }}>
                <StatusBar
                    backgroundColor='#2ECC71'
                    barStyle='light-content'
                />
                <SafeAreaView style={{ marginTop: StatusBar.currentHeight }}>
                    <ScrollView
                        showsVerticalScrollIndicator={false}
                    >
                        <View style={{ width: '100%', height: (width) / 8, backgroundColor: '#2ECC71' }}>
                            <View style={{ alignItems: 'center', justifyContent: 'space-between', flexDirection: 'row', padding: 15 }}>
                                <IonIcon name='ios-arrow-back' size={24} color='white' onPress={() => this.props.navigation.goBack()} />
                                <Text style={{ fontSize: 18, fontFamily: 'Montserrat-Bold', color: 'white' }}>Tarik Saldo</Text>
                                <Text style={{ fontSize: 18, fontFamily: 'Montserrat-Bold', color: 'white' }} />
                            </View>
                        </View>
                        <View style={{ padding: 15 }}>
                            <View style={{ width: '100%', height: (width) / 8, backgroundColor: 'white', borderRadius: 10, elevation: 1 }}>
                                <Item picker style={{ height: (width) / 8, borderRadius: 10 }}>
                                    <Picker
                                        mode='dropdown'
                                        iosIcon={<IonIcon name='ios-arrow-down' />}
                                        placeholderIconColor={<IonIcon name='ios-arrow-down' />}
                                        selectedValue={this.state.selectOption}
                                        onValueChange={(value) => this.setState({ selectOption: value })}
                                        style={{ width: undefined }}
                                    >
                                        <Picker.Item label="OVO" value="0" />
                                        <Picker.Item label="Gopay" value="1" />
                                        <Picker.Item label="Link Aja" value="2" />
                                    </Picker>
                                </Item>
                            </View>
                            <View style={{ width: '100%', height: (width) / 8, backgroundColor: 'white', borderRadius: 10, marginTop: 15, elevation: 1 }}>
                                <TextInput
                                    placeholder='Masukan nomor handphone'
                                    keyboardType='phone-pad'
                                    style={{ marginLeft: 10, marginRight: 10, fontSize: 15, fontFamily: 'Montserrat-SemiBold', color: '#333' }}
                                />
                            </View>
                            <View style={{ width: '100%', height: (width) / 8, backgroundColor: 'white', borderRadius: 10, marginTop: 15, elevation: 1 }}>
                                <TextInput
                                    placeholder='Masukan nominal'
                                    keyboardType='phone-pad'
                                    style={{ marginLeft: 10, marginRight: 10, fontSize: 15, fontFamily: 'Montserrat-SemiBold', color: '#333' }}
                                />
                            </View>
                            <View style={{ width: '100%', height: (width) / 8, backgroundColor: '#2ECC71', borderRadius: 30, marginTop: 15, elevation: 1 }}>
                                <TouchableRipple
                                    onPress={() => this.props.navigation.navigate('ProcessWithdraw')}
                                    rippleColor='#333'
                                    borderless={true}
                                    style={{ width: '100%', height: '100%', borderRadius: 30, alignItems: 'center', justifyContent: 'center' }}
                                >
                                    <Text style={{ fontSize: 15, fontFamily: 'Montserrat-Bold', color: 'white' }}>TARIK SALDO</Text>
                                </TouchableRipple>
                            </View>
                        </View>
                    </ScrollView>
                </SafeAreaView>
            </Container>
        );
    }
}

export default Withdraw;
