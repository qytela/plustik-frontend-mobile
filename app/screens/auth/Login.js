import React, { Component } from 'react';
import { View, Text, StatusBar, Image, ScrollView, Dimensions, TextInput, ActivityIndicator, ToastAndroid } from 'react-native';
import { Container } from 'native-base';
import { TouchableRipple } from 'react-native-paper';
import AsyncStorage from '@react-native-community/async-storage';
import NetInfo from '@react-native-community/netinfo';

import ModalInternet from '../../components/ModalInternet';
import ModalError from '../../components/ModalError';
import { Auth } from '../../services/API';

const { width, height } = Dimensions.get('window');

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            modalVisibleInternet: false,
            modalVisibleError: false,
            editable: true,
            email: '',
            password: ''
        };
    }

    componentDidMount() {
        this.navigationDidFocusListener = this.props.navigation.addListener('didFocus', () => {
            StatusBar.setBackgroundColor('transparent');
            StatusBar.setBarStyle('dark-content');
            StatusBar.setTranslucent(true);
        });
    }

    componentWillUnmount() {
        this.navigationDidFocusListener.remove();
    }

    onLoginPress = async () => {
        try {
            const { email, password } = this.state;

            if (email === '' || password === '') {
                return ToastAndroid.show(
                    'Field required',
                    ToastAndroid.BOTTOM,
                    ToastAndroid.LONG
                );
            }

            this.setState({
                isLoading: true,
                editable: false
            });

            const resJson = await Auth({ email, password });

            this.setState({
                isLoading: false,
                editable: true
            });

            if (resJson.status === true) {
                if (resJson.role !== 1) {
                    return ToastAndroid.show(
                        'Bad authentication',
                        ToastAndroid.BOTTOM,
                        ToastAndroid.Long
                    );
                }

                await AsyncStorage.setItem('TOKEN', resJson.tk);

                this.props.navigation.navigate('AppHomeStackNavigator');
            } else {
                ToastAndroid.show(
                    resJson.message,
                    ToastAndroid.BOTTOM,
                    ToastAndroid.LONG
                );
            }
        } catch (error) {
            const isConnected = await NetInfo.isConnected.fetch();
            if (!isConnected) {
                return this.setState({
                    isLoading: false,
                    modalVisibleInternet: true,
                    editable: true
                });
            }

            this.setState({
                isLoading: false,
                modalVisibleError: true,
                editable: true
            });
        }
    }

    onToggleModalInternet = (e) => {
        if (e === true) {
            this.setState({ modalVisibleInternet: false });
        }
    }

    onToggleModalError = (e) => {
        if (e === true) {
            this.setState({ modalVisibleError: false });
        }
    }

    render() {
        return (
            <Container>
                <ModalInternet
                    isVisible={this.state.modalVisibleInternet}
                    callback={this.onToggleModalInternet}
                />
                <ModalError
                    isVisible={this.state.modalVisibleError}
                    callback={this.onToggleModalError}
                />
                <ScrollView
                    showsVerticalScrollIndicator={false}
                >
                    <Image source={require('../../images/waves/waves-plustik.png')} style={{ width: (width) / 1, height: (width) / 1.7 }} />
                    <View style={{ padding: 15 }}>
                        <View style={{ marginTop: 0 }}>
                            <View style={{ alignItems: 'baseline', justifyContent: 'center' }}>
                                <Text style={{ fontSize: 15, fontFamily: 'Montserrat-Light', color: '#333' }}>Email</Text>
                            </View>
                            <View style={{ width: '100%', height: (width) / 7.5, alignItems: 'center', flexDirection: 'row', borderBottomColor: '#333', borderBottomWidth: 1 }}>
                                <Image source={require('../../images/icons/email-icon.png')} style={{ width: (width) / 15, height: (width) / 15, resizeMode: 'contain' }} />
                                <TextInput
                                    keyboardType='email-address'
                                    returnKeyType='next'
                                    editable={this.state.editable}
                                    onSubmitEditing={() => this.inputPassword.focus()}
                                    onChangeText={(email) => this.setState({ email })}
                                    style={{ width: '90%', height: (width) / 7.5, marginLeft: 10, fontSize: 15, fontFamily: 'Montserrat-Bold', color: '#333' }}
                                />
                            </View>
                        </View>
                        <View style={{ marginTop: 15 }}>
                            <View style={{ alignItems: 'baseline', justifyContent: 'center' }}>
                                <Text style={{ fontSize: 15, fontFamily: 'Montserrat-Light', color: '#333' }}>Password</Text>
                            </View>
                            <View style={{ width: '100%', height: (width) / 7.5, alignItems: 'center', flexDirection: 'row', borderBottomColor: '#333', borderBottomWidth: 1 }}>
                                <Image source={require('../../images/icons/key-icon.png')} style={{ width: (width) / 15, height: (width) / 15, resizeMode: 'contain' }} />
                                <TextInput
                                    ref={(ref) => this.inputPassword = ref}
                                    secureTextEntry={true}
                                    editable={this.state.editable}  
                                    onChangeText={(password) => this.setState({ password })}
                                    style={{ width: '90%', height: (width) / 7.5, marginLeft: 10, fontSize: 15, fontFamily: 'Montserrat-Bold', color: '#333' }}
                                />
                            </View>
                        </View>
                        <View style={{ marginTop: 30, alignItems: 'center', justifyContent: 'center' }}>
                            <View style={{ width: '90%', height: (width) / 7.5, alignItems: 'center', justifyContent: 'center', backgroundColor: '#2ECC71', borderRadius: 30 }}>
                                <TouchableRipple
                                    onPress={() => this.onLoginPress()}
                                    rippleColor='#333'
                                    borderless={true}
                                    disabled={this.state.isLoading}
                                    style={{ width: '100%', height: '100%', borderRadius: 30, alignItems: 'center', justifyContent: 'center' }}
                                >
                                    {
                                        this.state.isLoading
                                            ?   <ActivityIndicator size='large' color='white' />
                                            :   <Text style={{ fontSize: 20, fontFamily: 'Montserrat-Bold', color: 'white' }}>Masuk</Text>
                                    }
                                </TouchableRipple>
                            </View>
                        </View>
                        <View style={{ marginTop: 10, marginBottom: 10 }}>
                            <Text style={{ fontSize: 13, fontFamily: 'Montserrat-Light', color: '#333', textAlign: 'center' }}>Belum memiliki akun ? <Text style={{ color: '#23CC71' }} onPress={() => this.props.navigation.navigate('Register')}>Daftar</Text> dulu yuk</Text>
                        </View>
                    </View>
                </ScrollView>
            </Container>
        );
    }
}

export default Login;
