import React, { Component } from 'react';
import { View, Text, StatusBar, ScrollView, Image, Dimensions, TextInput, ToastAndroid, ActivityIndicator } from 'react-native';
import { Container } from 'native-base';
import { TouchableRipple } from 'react-native-paper';
import NetInfo from '@react-native-community/netinfo';

import ModalInternet from '../../components/ModalInternet';
import ModalError from '../../components/ModalError';
import { RegisterUser } from '../../services/API';

const { width, height } = Dimensions.get('window');

class Register extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            modalVisibleInternet: false,
            modalVisibleError: false,
            editable: true,
            fullname: '',
            phone_number: '',
            email: '',
            address: '',
            password: '',
            c_password: ''
        };
    }

    componentDidMount() {
        this.navigationDidFocusListener = this.props.navigation.addListener('didFocus', () => {
            StatusBar.setBackgroundColor('transparent');
            StatusBar.setBarStyle('dark-content');
            StatusBar.setTranslucent(true);
        });
    }

    componentWillUnmount() {
        this.navigationDidFocusListener.remove();
    }

    onRegisterPress = async () => {
        try {
            const { fullname, phone_number, email, address, password, c_password } = this.state;
            
            if (fullname === '' || phone_number === '' || email === '' || address === '' || password === '' || c_password === '') {
                return ToastAndroid.show(
                    'Field required',
                    ToastAndroid.BOTTOM,
                    ToastAndroid.LONG
                );
            }
            if (password !== c_password) {
                return ToastAndroid.show(
                    'Password do not match',
                    ToastAndroid.BOTTOM,
                    ToastAndroid.LONG
                );
            }

            this.setState({
                isLoading: true,
                editable: false
            });

            const resJson = await RegisterUser({ fullname, phone_number, email, address, password, c_password });

            this.setState({
                isLoading: false,
                editable: true
            });

            if (resJson.status === true) {
                ToastAndroid.show(
                    resJson.message,
                    ToastAndroid.BOTTOM,
                    ToastAndroid.LONG
                );

                this.props.navigation.navigate('Login');
            } else {
                ToastAndroid.show(
                    resJson.message,
                    ToastAndroid.BOTTOM,
                    ToastAndroid.LONG
                );
            }
        } catch (error) {
            const isConnected = await NetInfo.isConnected.fetch();
            if (!isConnected) {
                return this.setState({
                    isLoading: false,
                    modalVisibleInternet: true,
                    editable: true
                });
            }

            this.setState({
                isLoading: false,
                modalVisibleError: true,
                editable: true
            });
        }
    }

    onToggleModalInternet = (e) => {
        if (e === true) {
            this.setState({ modalVisibleInternet: false });
        }
    }

    onToggleModalError = (e) => {
        if (e === true) {
            this.setState({ modalVisibleError: false });
        }
    }

    render() {
        return (
            <Container>
                <ModalInternet
                    isVisible={this.state.modalVisibleInternet}
                    callback={this.onToggleModalInternet}
                />
                <ModalError
                    isVisible={this.state.modalVisibleError}
                    callback={this.onToggleModalError}
                />
                <ScrollView
                    showsVerticalScrollIndicator={false}
                >
                    <Image source={require('../../images/waves/waves-plustik.png')} style={{ width: (width) / 1, height: (width) / 1.7 }} />
                    <View style={{ padding: 15 }}>
                        <View style={{ marginTop: 0 }}>
                            <View style={{ alignItems: 'baseline', justifyContent: 'center' }}>
                                <Text style={{ fontSize: 15, fontFamily: 'Montserrat-Light', color: '#333' }}>Nama Lengkap</Text>
                            </View>
                            <View style={{ width: '100%', height: (width) / 7.5, alignItems: 'center', flexDirection: 'row', borderBottomColor: '#333', borderBottomWidth: 1 }}>
                                <Image source={require('../../images/icons/user-icon.png')} style={{ width: (width) / 15, height: (width) / 15, resizeMode: 'contain' }} />
                                <TextInput
                                    returnKeyType='next'
                                    editable={this.state.editable}
                                    onSubmitEditing={() => this.inputPhone.focus()}
                                    onChangeText={(fullname) => this.setState({ fullname })}
                                    style={{ width: '90%', height: (width) / 7.5, marginLeft: 10, fontSize: 15, fontFamily: 'Montserrat-Bold', color: '#333' }}
                                />
                            </View>
                        </View>
                        <View style={{ marginTop: 15 }}>
                            <View style={{ alignItems: 'baseline', justifyContent: 'center' }}>
                                <Text style={{ fontSize: 15, fontFamily: 'Montserrat-Light', color: '#333' }}>Nomor Handphone</Text>
                            </View>
                            <View style={{ width: '100%', height: (width) / 7.5, alignItems: 'center', flexDirection: 'row', borderBottomColor: '#333', borderBottomWidth: 1 }}>
                                <Image source={require('../../images/icons/phone-icon.png')} style={{ width: (width) / 15, height: (width) / 15, resizeMode: 'contain' }} />
                                <TextInput
                                    ref={(ref) => this.inputPhone = ref}
                                    returnKeyType='next'
                                    keyboardType='number-pad'
                                    editable={this.state.editable}
                                    onSubmitEditing={() => this.inputEmail.focus()}
                                    onChangeText={(phone_number) => this.setState({ phone_number })}
                                    style={{ width: '90%', height: (width) / 7.5, marginLeft: 10, fontSize: 15, fontFamily: 'Montserrat-Bold', color: '#333' }}
                                />
                            </View>
                        </View>
                        <View style={{ marginTop: 15 }}>
                            <View style={{ alignItems: 'baseline', justifyContent: 'center' }}>
                                <Text style={{ fontSize: 15, fontFamily: 'Montserrat-Light', color: '#333' }}>Email</Text>
                            </View>
                            <View style={{ width: '100%', height: (width) / 7.5, alignItems: 'center', flexDirection: 'row', borderBottomColor: '#333', borderBottomWidth: 1 }}>
                                <Image source={require('../../images/icons/email-icon.png')} style={{ width: (width) / 15, height: (width) / 15, resizeMode: 'contain' }} />
                                <TextInput
                                    ref={(ref) => this.inputEmail = ref}
                                    keyboardType='email-address'
                                    returnKeyType='next'
                                    editable={this.state.editable}
                                    onSubmitEditing={() => this.inputAddress.focus()}
                                    onChangeText={(email) => this.setState({ email })}
                                    style={{ width: '90%', height: (width) / 7.5, marginLeft: 10, fontSize: 15, fontFamily: 'Montserrat-Bold', color: '#333' }}
                                />
                            </View>
                        </View>
                        <View style={{ marginTop: 15 }}>
                            <View style={{ alignItems: 'baseline', justifyContent: 'center' }}>
                                <Text style={{ fontSize: 15, fontFamily: 'Montserrat-Light', color: '#333' }}>Alamat</Text>
                            </View>
                            <View style={{ width: '100%', height: (width) / 7.5, alignItems: 'center', flexDirection: 'row', borderBottomColor: '#333', borderBottomWidth: 1 }}>
                                <Image source={require('../../images/icons/placeholder-icon.png')} style={{ width: (width) / 15, height: (width) / 15, resizeMode: 'contain' }} />
                                <TextInput
                                    ref={(ref) => this.inputAddress = ref}
                                    returnKeyType='next'
                                    editable={this.state.editable}
                                    onSubmitEditing={() => this.inputPassword.focus()}
                                    onChangeText={(address) => this.setState({ address })}
                                    style={{ width: '90%', height: (width) / 7.5, marginLeft: 10, fontSize: 15, fontFamily: 'Montserrat-Bold', color: '#333' }}
                                />
                            </View>
                        </View>
                        <View style={{ marginTop: 15 }}>
                            <View style={{ alignItems: 'baseline', justifyContent: 'center' }}>
                                <Text style={{ fontSize: 15, fontFamily: 'Montserrat-Light', color: '#333' }}>Password</Text>
                            </View>
                            <View style={{ width: '100%', height: (width) / 7.5, alignItems: 'center', flexDirection: 'row', borderBottomColor: '#333', borderBottomWidth: 1 }}>
                                <Image source={require('../../images/icons/key-icon.png')} style={{ width: (width) / 15, height: (width) / 15, resizeMode: 'contain' }} />
                                <TextInput
                                    ref={(ref) => this.inputPassword = ref}
                                    secureTextEntry={true}
                                    returnKeyType='next'
                                    editable={this.state.editable}
                                    onSubmitEditing={() => this.inputCPassword.focus()}
                                    onChangeText={(password) => this.setState({ password })}
                                    style={{ width: '90%', height: (width) / 7.5, marginLeft: 10, fontSize: 15, fontFamily: 'Montserrat-Bold', color: '#333' }}
                                />
                            </View>
                        </View>
                        <View style={{ marginTop: 15 }}>
                            <View style={{ alignItems: 'baseline', justifyContent: 'center' }}>
                                <Text style={{ fontSize: 15, fontFamily: 'Montserrat-Light', color: '#333' }}>Confirm Password</Text>
                            </View>
                            <View style={{ width: '100%', height: (width) / 7.5, alignItems: 'center', flexDirection: 'row', borderBottomColor: '#333', borderBottomWidth: 1 }}>
                                <Image source={require('../../images/icons/key-icon.png')} style={{ width: (width) / 15, height: (width) / 15, resizeMode: 'contain' }} />
                                <TextInput
                                    ref={(ref) => this.inputCPassword = ref}
                                    secureTextEntry={true}
                                    editable={this.state.editable}
                                    onChangeText={(c_password) => this.setState({ c_password })}
                                    style={{ width: '90%', height: (width) / 7.5, marginLeft: 10, fontSize: 15, fontFamily: 'Montserrat-Bold', color: '#333' }}
                                />
                            </View>
                        </View>
                        <View style={{ marginTop: 30, alignItems: 'center', justifyContent: 'center' }}>
                            <View style={{ width: '90%', height: (width) / 7.5, alignItems: 'center', justifyContent: 'center', backgroundColor: '#2ECC71', borderRadius: 30 }}>
                                <TouchableRipple
                                    onPress={() => this.onRegisterPress()}
                                    rippleColor='#333'
                                    borderless={true}
                                    disabled={this.state.isLoading}
                                    style={{ width: '100%', height: '100%', borderRadius: 30, alignItems: 'center', justifyContent: 'center' }}
                                >
                                    {
                                        this.state.isLoading
                                                ?   <ActivityIndicator size='large' color='white' />
                                                :   <Text style={{ fontSize: 20, fontFamily: 'Montserrat-Bold', color: 'white' }}>Daftar</Text>
                                    }
                                </TouchableRipple>
                            </View>
                        </View>
                        <View style={{ marginTop: 10, marginBottom: 10 }}>
                            <Text style={{ fontSize: 13, fontFamily: 'Montserrat-Light', color: '#333', textAlign: 'center' }}>Sudah memiliki akun ? <Text style={{ color: '#23CC71' }} onPress={() => this.props.navigation.navigate('Login')}>Masuk</Text></Text>
                        </View>
                    </View>
                </ScrollView>
            </Container>
        );
    }
}

export default Register;
