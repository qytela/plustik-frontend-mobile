import Geolocation from 'react-native-geolocation-service';
import Permissions from 'react-native-permissions';

export const ReqEnableGPS = () => {
    return new Promise((resolve, reject) => {
        Geolocation.getCurrentPosition(
            (position) => {
                resolve(position.coords);
            },
            (error) => {
                reject(error)
            },
            {
                enableHighAccuracy: true,
                timeout: 5000000
            }
        );
    });
}

export const ReqPermissionLocation = async () => {
    try {
        const reqPermission = await Permissions.request('location');
        switch (reqPermission) {
            case 'authorized':
                const reqGPS = await ReqEnableGPS();
                if (reqGPS.latitude && reqGPS.longitude) {
                    return reqGPS;
                } else {
                    return false;
                }
            default:
                return false;
        }
    } catch (error) {
        return error;
    }
}