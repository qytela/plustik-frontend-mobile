export const BASE_URL = 'https://plustik-api.herokuapp.com';
const BASE_PATH = 'api/v1'

// Login User
export const Auth = ({ email, password }) => {
    return new Promise((resolve, reject) => {
        fetch(`${BASE_URL}/${BASE_PATH}/users/login`, {
            method: 'POST',
            body: JSON.stringify({ email, password }),
            headers: {
                'Content-type': 'Application/json'
            }
        })
        .then((response) => response.json())
        .then((response) => resolve(response))
        .catch((error) => reject(error));
    });
}

export const RegisterUser = ({ fullname, phone_number, email, address, password, c_password }) => {
    return new Promise((resolve, reject) => {
        fetch(`${BASE_URL}/${BASE_PATH}/users/register`, {
            method: 'POST',
            body: JSON.stringify({ fullname, phone_number, email, address, password, c_password }),
            headers: {
                'Content-type': 'Application/json'
            }
        })
        .then((response) => response.json())
        .then((response) => resolve(response))
        .catch((error) => reject(error));
    });
}

// GraphQL
export const GraphQL = ({ token, query }) => {
    return new Promise((resolve, reject) => {
        fetch(`${BASE_URL}/graphql`, {
            method: 'POST',
            body: JSON.stringify({ query }),
            headers: {
                'Content-type': 'Application/json',
                'Authorization': `Bearer ${token}`
            }
        })
        .then((response) => response.json())
        .then((response) => resolve(response))
        .catch((error) => reject(error));
    });
}