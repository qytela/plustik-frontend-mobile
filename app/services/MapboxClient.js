export const MAPBOX_API_KEY = 'pk.eyJ1IjoicXl0ZWxhNTU4IiwiYSI6ImNrMWE1ajJpcjA5djkzY3BmbG1vMTdlcXMifQ.ZzMAIzluNmPRHK_bzgXm0w';

export const RetrieveDirection = (profile, startPoint, endPoint, geometries) => {
    return new Promise((resolve, reject) => {
        fetch(`https://api.mapbox.com/directions/v5/mapbox/${profile}/${startPoint};${endPoint}?geometries=${geometries}&access_token=${MAPBOX_API_KEY}`, {
            method: 'GET',
            headers: {
                'Content-type': 'Application/json'
            }
        })
        .then(response => response.json())
        .then(response => resolve(response))
        .catch(error => reject(error));
    });
}

export const ForwardGeocoding = (search) => {
    return new Promise((resolve, reject) => {
        fetch(`https://api.mapbox.com/geocoding/v5/mapbox.places/${search}.json?language=ID&country=ID&access_token=${MAPBOX_API_KEY}`, {
            method: 'GET',
            headers: {
                'Content-type': 'Application/json'
            }
        })
        .then(response => response.json())
        .then(response => resolve(response))
        .catch(error => reject(error));
    });
}