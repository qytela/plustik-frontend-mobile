import { createStore } from 'redux';
import { persistStore, persistCombineReducers } from 'redux-persist';
import AsyncStorage from '@react-native-community/async-storage';

import rootReducer from '../reducers';

const config = {
    key: 'primary',
    storage: AsyncStorage
}

const persistedReducer = persistCombineReducers(config, rootReducer);

export default () => {
    const store = createStore(persistedReducer);
    const persistor = persistStore(store);

    return {
        store,
        persistor
    };
}