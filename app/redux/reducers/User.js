/**
 * user: {
 *      fullname: string
 *      address: string
 * }
 */

const initialState = {
    user: {}
}

const user = (state = initialState, action) => {
    switch (action.type) {
        case 'UPDATE_USER':
            return state['user'] = action.data;
        case 'CLEAN_USER':
            return state['user'] = {};
        default:
            return state;
    }
}

export default user;