/**
 * myLocation: [lng, lat]
 */

const initialState = {
    myLocation: []
}

const location = (state = initialState, action) => {
    switch (action.type) {
        case 'UPDATE_LOCATION':
            return {
                ...state,
                myLocation: [action.longitude, action.latitude]
            };
        default:
            return state;
    }
}

export default location;