/**
 * setData: {
 *      coordinates: [lng, lat]
 *      place_name: string
 *      uri: string (base64)
 * }
 */

const initialState = {
    setData: {}
}

const data = (state = initialState, action) => {
    switch (action.type) {
        case 'UPDATE_DATA':
            return state['setData'] = action.data
        case 'CLEAN_DATA':
            return state['setData'] = {};
        default:
        return state;
    }
}

export default data;