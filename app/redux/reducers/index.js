import data from './Data';
import location from './Location';
import user from './User';

export default {
    data,
    location,
    user
}